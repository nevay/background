import time

import pylab
import scipy.io
import cothread
import cothread.catools

# /home/atf2-fs/ATF2/FlightSim/coreApps/FlECS.m

bumps = [ \
{"name":"QD10X","dir":"x","mags":["ZH3X","ZH4X","ZH5X","ZH6X"]},\
{"name":"QD10X","dir":"y","mags":["ZV6X","ZV7X","ZV8X","ZV9X"]},\
{"name":"QF11X","dir":"x","mags":["ZH4X","ZH5X","ZH6X","ZH7X"]},\
{"name":"QF11X","dir":"y","mags":["ZV6X","ZV7X","ZV8X","ZV9X"]},\
{"name":"QD12X","dir":"x","mags":["ZH4X","ZH5X","ZH6X","ZH7X"]},\
{"name":"QD12X","dir":"y","mags":["ZV7X","ZV8X","ZV9X","ZV10X"]},\
{"name":"QD16X","dir":"x","mags":["ZH6X","ZH7X","ZH8X","ZH9X"]},\
{"name":"QD16X","dir":"y","mags":["ZV8X","ZV9X","ZV10X","ZV11X"]},\
{"name":"QF17X","dir":"x","mags":["ZH7X","ZH8X","ZH9X","ZH10X"]},\
{"name":"QF17X","dir":"y","mags":["ZV8X","ZV9X","ZV10X","ZV11X"]},\
{"name":"QD18X","dir":"x","mags":["ZH7X","ZH8X","ZH9X","ZH10X"]},\
{"name":"QD18X","dir":"y","mags":["ZV9X","ZV10X","ZV11X","ZV1FF"]},\
{"name":"QF19X","dir":"x","mags":["ZH8X","ZH9X","ZH10X","ZH1FF"]},\
{"name":"QF19X","dir":"y","mags":["ZV9X","ZV10X","ZV11X","ZV1FF"]},\
{"name":"QD20X","dir":"x","mags":["ZH8X","ZH9X","ZH10X","ZH1FF"]},\
{"name":"QD20X","dir":"y","mags":["ZV10X","ZV11X","ZV1FF","QD10BFF"]},\
{"name":"QF21X","dir":"x","mags":["ZH9X","ZH10X","ZH1FF","QD10BFF"]},\
{"name":"QF21X","dir":"y","mags":["ZV10X","ZV11X","ZV1FF","QD10BFF"]},\
{"name":"QM16FF","dir":"x","mags":["ZH9X","ZH10X","ZH1FF","QD10BFF"]},\
{"name":"IPA","dir":"x","mags":["QF7FF","QD0FF","DUMP","DUMP"]},\
{"name":"IPB","dir":"x","mags":["QF7FF","QD0FF","DUMP","DUMP"]},\
{"name":"IPA","dir":"y","mags":["QF7FF","QD0FF","DUMP","DUMP"]},\
{"name":"IPB","dir":"y","mags":["QF7FF","QD0FF","DUMP","DUMP"]}]

bpms = ["QF1X","QD2X","QF3X","QF4X","QD5X","QF6X","QF7X","QF9X","QF13X","QD14X","QF15X","QD10X","QF11X","QD12X","QD16X","QF17X","QD18X","QF19X","QD20X","QF21X","IPT1","IPT2","IPT3","IPT4","QM16FF","QM15FF","QM14FF","FB2FF","QM13FF","QM12FF","QM11FF","QD10BFF","QD10AFF","QF9BFF","SF6FF","QF9AFF","QD8FF","QF7FF","QD6FF","QF5BFF","SF5FF","QF5AFF","QD4BFF","SD4FF","QD4AFF","QF3FF","QD2BFF","QD2AFF","SF1FF","QF1FF","SD0FF","QD0FF","PREIP","IPA","IPB","M-PIP","REFC1","REFC2","REFC3","CDIODE","REFS1","REFS2","SDIODE","SPHASE","REFIPX","REFIPY1","REFIPY2","REFIPTX","REFIPTY","IPDIODE","TILT","XBAND"]


class BeamLine :
    def __init__(self,
                 fileName = "./BEAMLINE.mat") :
        f = scipy.io.loadmat(fileName) 
        bl = f['BEAMLINE']
        
        self.Name = []
        self.L    = []
        self.Class= []

        ind = 0
        for e in pylab.flatten(bl) :

            name  = e.Name[0]
            cls   = e.Class[0]
            if cls != 'MARK' : 
                L = e.L[0][0]
            else :
                L = 0
                
            self.Name.append(name)
            self.Class.append(cls)
            self.L.append(L)

            ind = ind + 1 

        self.Ind = pylab.arange(0,len(self.Name),1) 

    def findEleByName(self,name) :
        return self.Ind[pylab.array(self.Name) == name]
            
    def bumpDefs(self) :
        pass
    
class FSEpics :
    def __init__(self, bOnline = True) :
        print 'FlightSimulator:FSEpics:__init__>'
        self.QuitEvent = cothread.Event(auto_reset = False)

        self.bl = BeamLine()
        self.rmat = []
        self.bpms  = ['QD10X',
                      'QF11X',
                      'QD12X',
                      'QD16X',
                      'QF17X',
                      'QD18X',
                      'QF19X',
                      'QD20X',
                      'QF21X']
        self.corrs = ['ZH3X',
                      'ZV6X',
                      'ZH4X',
                      'ZV7X',
                      'ZH5X',
                      'ZV8X',
                      'ZH6X',
                      'ZH7X',
                      'ZV9X',
                      'ZH8X',
                      'ZV10X',
                      'ZH9X',
                      'ZV11X',
                      'ZH10X',
                      'ZH1FF',
                      'ZV1FF']
        self.quadsWithBpms  = ['QD10X','QF11X','QD12X','QD16X','QF17X','QD18X',
                               'QF19X','QD20X','QF21X','QM16FF','QM15FF','QM14FF','QM13FF',
                               'QM12FF','QM11FF','QD10BFF','QD10AFF','QF9BFF','SF6FF','QF9AFF',
                               'QD8FF','QF7FF','QD6FF','QF5BFF','SF5FF','QF5AFF','QD4BFF',
                               'SD4FF','QD4AFF','QF3FF','QD2BFF','QD2AFF','SF1FF','SD0FF']
        self.bpms           = ['MQD10X','MQF11X','MQD12X','MQD16X','MQF17X','MQD18X',
                               'MQF19X','MQD20X','MQF21X','MQM16FF','MQM15FF','MQM14FF',
                               'MFB2FF','MQM13FF','MQM12FF','MQM11FF','MQD10BFF','MQD10AFF',
                               'MQF9BFF','MSF6FF','MQF9AFF','MQD8FF','MQF7FF','MQD6FF','MQF5BFF',
                               'MSF5FF','MQF5AFF','MQD4BFF','MSD4FF','MQD4AFF','MQF3FF',
                               'MQD2BFF','MQD2AFF','MSF1FF','MQF1FF','MSD0FF','MQD0FF',
                               'MPREIP','IPBPMA','IPBPMB','MPIP']
        self.corrs          = ['ZV1X','ZV2X','ZH1X','ZV3X','ZH2X','ZV4X','ZV5X','ZH3X',
                               'ZV6X' ,'ZH4X','ZV7X','ZH5X','ZV8X','ZH6X','ZH7X','ZV9X',
                               'ZH8X','ZV10X','ZH9X','ZV11X','ZH10X','ZH1FF','ZV1FF']

        if bOnline : 
            try :
                cothread.catools.caput("FSECS:command","nmCavBpmBumpInit");
                cothread.catools.ca_flush_io();
                time.sleep(5)
                
                # try a few gets to sort things out
                self.getInd("QD10X")
                self.getInd("QD10X")
                self.getInd("QD10X")                
            except :
                print 'FlightSimulator:FSEpics> problem starting FS'

        # data storage
        self.rmat = []
        self.kicks = []
        self.rmatMagBpmScale = []
        self.deltaSMagBpmScale = [] 

        # first entry for ipbumps
        self.first = True

    def test(self,list = []) :
        if list == [] :
            list = self.bpms
        
        for e in list :
            print e,self.getInd(e)

    def testQuads(self) :
        for e in self.quadsWithBpms :
            i1 = self.getInd(e)
            i2 = self.getInd('M'+e)
            back = 0
            if i2 > i1 :
                back = 1 
            print e,i1,i2,back
            
    def log(self, corr = 'ZV1X') :
       # open file
        t = time.strftime('%Y%m%d_%H%M%S')
        self.filename = '/atf/data/cbpm/raw/bpmFs_'+t+'_'+corr+'.dat'

        # Get magBpmScale rmats 
        self.magBpmScale()
        self.corrScanData(corr)
        
        f = open(self.filename,'w')
        self.write(f)
        
#        for c in self.corrs : 
#            self.corrScanData(c)
#            self.write(f)

        f.close()
        
        return self.filename
        
    def magBpmScale(self) :
        print 'FlightSimulator:FSEpics:magBpmScale> Rmat quad <--> bpm'

        self.rmatMagBpmScale   = []
        self.deltaSMagBpmScale = [] 
                
        for e in self.quadsWithBpms :
            i1   = self.bl.findEleByName(e)[0]+1
            i2   = self.bl.findEleByName('M'+e)[0]+1

            if i2 > i1 :                           
                rmat = self.getRmatInd(i1,i2)
            else :
                rmat = [1.0,0.0,0.0,0.0,0.0,0.0,
                        0.0,1.0,0.0,0.0,0.0,0.0,
                        0.0,0.0,1.0,0.0,0.0,0.0,
                        0.0,0.0,0.0,1.0,0.0,0.0,
                        0.0,0.0,0.0,0.0,1.0,0.0,
                        0.0,0.0,0.0,0.0,0.0,1.0]

            print 'FlightSimulator:FSEpics:magBpmScale> '+e
            self.rmatMagBpmScale.append(rmat)
            smag = self.getEleInfo(e,"S")
            sbpm = self.getEleInfo("M"+e,"S")
            self.deltaSMagBpmScale.append(sbpm-smag)
                
    def corrScanData(self,corr) :
        print 'FlightSimulator:FSEpics:corrScanData> Rmat'
        self.corr = corr
        self.rmat = []
        for b in self.bpms :
            rmat = self.getRmat(corr,b)
            print 'FlightSimulator:FSEpics:corrScanData>',corr,b
            self.rmat.append(rmat)
        self.rmat = pylab.array(self.rmat)
        print 'FlightSimulator:FSEpics> Rmat done'

        print 'FlightSimulator:FSEpics:corrScanData> kicks'
        self.kicks = []
        for c in self.corrs :
            k = self.getKick(c)
            self.kicks.append(k)
            print 'FlightSimulator:FSEpics:corrScanData>',c,k
        print 'FlightSimulator:FSEpics:corrScanData> kicks done'

    def getRmatInd(self, i1, i2) :
        cothread.catools.caput("FSECS:arrCmd",[i1,i2])
        cothread.catools.caput("FSECS:command","GetRmat")
        cothread.catools.ca_flush_io()
        time.sleep(1.25)
        
        rmat = cothread.catools.caget("FSECS:arrResp",count=36)
        return rmat
        
    def getRmat(self,e1Name,e2Name) :
        i1 = self.getInd(e1Name)
        i2 = self.getInd(e2Name)
        cothread.catools.caput("FSECS:arrCmd",[i1,i2])
        cothread.catools.caput("FSECS:command","GetRmat")
        cothread.catools.ca_flush_io()
        time.sleep(1.25)
        
        rmat = cothread.catools.caget("FSECS:arrResp",count=36)
        return rmat
    
    def getKick(self,eName) :
        i1 = self.getInd(eName)
        cothread.catools.caput("FSECS:valCmd",i1)
        cothread.catools.caput("FSECS:command","GetAmpConv")
        cothread.catools.ca_flush_io()
        time.sleep(1.5)
        
        kick = cothread.catools.caget("FSECS:valResp")
        return kick

    def getInd(self,name) :
        cothread.catools.caput("FSECS:strCmd",name)
        cothread.catools.caput("FSECS:command","GetIndByName")
        cothread.catools.ca_flush_io()        
        time.sleep(1.0)
        
        i1 = cothread.catools.caget("FSECS:valResp")
        
        return int(i1)

    def getEleInfo(self, eleName, infoName) :
        i = self.getInd(eleName)
        cothread.catools.caput("FSECS:valCmd",i)
        cothread.catools.caput("FSECS:strCmd",infoName)
        cothread.catools.caput("FSECS:command","getelefield")
        cothread.catools.ca_flush_io()        
        time.sleep(1.0)

        val = cothread.catools.caget("FSECS:valResp")
        return val

    def setCorrector(self,name,value) :
        cothread.catools.caput("FSECS:strCmd",name)
        cothread.catools.caput("FSECS:valCmd",value)
        cothread.catools.caput("FSECS:command","setps")
        cothread.catools.ca_flush_io()
        time.sleep(3)

    def getCorrector(self,name) :
        cothread.catools.caput("FSECS:strCmd",name)
        cothread.catools.caput("FSECS:command","getps")
        cothread.catools.ca_flush_io()
        time.sleep(1.0)
        d = cothread.catools.caget("FSECS:valResp")
        return d

    def setBump(self, bpmName, value) :
        print 'FlightSimulator:setBump> ',bpmName, value

        if self.first and (bpmName == 'MM-PIPx' or
                      bpmName == 'MM-PIPy' or
                      bpmName == 'MPREIPx' or
                      bpmName == 'MPREIPy' or
                      bpmName == 'IPAx' or
                      bpmName == 'IPAy' or
                      bpmName == 'IPBx' or
                      bpmName == 'IPBy') :                      
            cothread.catools.caput("FSECS:command","bumpipinit")
            cothread.catools.ca_flush_io()   

            self.first = False
            self.QuitEvent.Reset()
            self.m = cothread.catools.camonitor("FSECS:command",self.fsecsCommandCallback)

            try :
                self.QuitEvent.Wait()
            except KeyboardInterrupt :
                self.QuitEvent.Signal()
                 
        if bpmName == 'MM-PIPx' :
            print 'setBump M-PIP x'
            cothread.catools.caput("FSECS:strCmd","x")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpMPIP")            
        elif bpmName == 'MM-PIPy' :
            print 'setBump M-PIP y'            
            cothread.catools.caput("FSECS:strCmd","y")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpMPIP")            
        elif bpmName == 'MPREIPx' :
            print 'setBump MPREIP x'
            cothread.catools.caput("FSECS:strCmd","x")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpMPREIP")
        elif bpmName == 'MPREIPy' :
            print 'setBump MPREIP y'
            cothread.catools.caput("FSECS:strCmd","y")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpMPREIP")
        elif bpmName == 'MIPAy' : 
            print 'setBump IPBPMA y'
            cothread.catools.caput("FSECS:strCmd","y")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpIPBPMA")
        elif bpmName == 'MIPBy' :
            print 'setBump IPBPMB y'
            cothread.catools.caput("FSECS:strCmd","y")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpIPBPMB")
        elif bpmName == 'MIPAx' : 
            print 'setBump IPBPMA x'
            cothread.catools.caput("FSECS:strCmd","x")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpIPBPMA")
        elif bpmName == 'MIPBx' :
            print 'setBump IPBPMB x'
            cothread.catools.caput("FSECS:strCmd","x")
            cothread.catools.caput("FSECS:valCmd",value)
            cothread.catools.caput("FSECS:command","bumpIPBPMB")            
        else :
            print 'setBump Other : ',bpmName   
            cothread.catools.caput("FSECS:strCmd",bpmName);        
            print 'bpmName ',bpmName
            cothread.catools.caput("FSECS:valCmd",value);
            print 'value ',value
            cothread.catools.caput("FSECS:command","nmCavBpmBump")

        cothread.catools.ca_flush_io()        

        self.QuitEvent.Reset()
        self.m = cothread.catools.camonitor("FSECS:command",self.fsecsCommandCallback)

        try :
            self.QuitEvent.Wait()
        except KeyboardInterrupt :
            self.QuitEvent.Signal()
            
        time.sleep(2)

        self.m.close()
       
    def fsecsCommandCallback(self, value) :
#        print 'callback ',value
        if value == ' ' :
#            print 'callback ',value
            self.QuitEvent.Signal()


    def beamCoordinates(self,bpmindex = 0, cval = [0.,0.]) :
        # beam vector            
        v  = [0.,cval[0],0.,cval[1],0.,0.]
        # response matrix
        rm = pylab.reshape(self.rmat[bpmindex],[6,6])
        # new coordinate
        vp = pylab.dot(rm,v)
        print v
        print rm
        print vp
        return vp
        
    def write(self,f) :
        print 'FlightSimulator:FSEpics:write>'
        ind = 0
        for rm in self.rmat :
            f.write('rmat> ')
            f.write(str(ind)+' '+self.corr+' '+self.bpms[ind]+' ')
            for v in self.rmat[ind] :
                f.write(str(v)+' ')
            f.write('\n')
            
            ind = ind+1

        ind = 0 
        for k in self.kicks :
            f.write('kick> ')
            f.write(str(ind)+' '+self.corrs[ind]+' '+str(k)+'\n')        
            ind = ind + 1

        ind = 0
        for rm in self.rmatMagBpmScale :
            f.write('rmatMagBpmScale> '+self.quadsWithBpms[ind]+' '+str(self.deltaSMagBpmScale[ind])+' ')

            for v in self.rmatMagBpmScale[ind] :
                f.write(str(v)+' ')
            f.write('\n')
            ind = ind +1 
                    
    def read(self,fd) :
        print 'FlightSimulator:FSEpics:read>'
        ind = 0
        self.iread = 0    
        print 'FlightSimulator:FSEpics:read>',fd
        
        print 'FlightSimulator:FSEpics:read> RMats'
        for i in range(0,len(self.bpms)) :
            l = fd.readline()
            sl = l.split(" ")
            rm = []
            if sl[0] == "rmat>" :
                for i in range(4,len(sl)-1) :
                    rm.append(float(sl[i]))
            self.rmat.append(rm)
        self.rmat = pylab.array(self.rmat)

        print 'FlightSimulator:FSEpics:read> Kicks'
        for i in range(0,len(self.corrs)) :
            l = fd.readline()
            sl = l.split(" ")
            kf = []
            if sl[0] == 'kick>' :
                ind  = sl[1] 
                corr = sl[2]
                k    = float(sl[3])
            self.kicks.append(k)

        print 'FlightSimulator:FSEpics:read> rmatQuadsWithBpms'
        for i in range(0,len(self.quadsWithBpms)) :
            l = fd.readline()
            l = l.strip(' \n')
            sl = l.split(" ")
            rmatMagBpmScale = []
            if sl[0] == 'rmatMagBpmScale>' :
                mag = sl[1]
                ds  = float(sl[2])
                for v in sl[3:] :
                    rmatMagBpmScale.append(float(v))

                self.deltaSMagBpmScale.append(ds)
                self.rmatMagBpmScale.append(rmatMagBpmScale)
                
    def array(self) :
        self.kicks            = pylab.array(self.kicks)
        self.rmat             = pylab.array(self.rmat)
        self.rmatMagBpmScale  = pylab.array(self.rmatMagBpmScale)
        self.deltaSMagBpmScale= pylab.array(self.deltaSMagBpmScale)

    def bpmMagScaleErrors(self) :
        self.array()

        bpmxError = []
        bpmyError = [] 
        
        for b in self.quadsWithBpms :

            ind = self.quadsWithBpms.index(b)
            rmat = pylab.reshape(self.rmatMagBpmScale[ind],(6,6))

            quadd = 100e-6

            bpmx  = rmat[0,0]*quadd+rmat[0,1]*quadd*pylab.fabs(self.deltaSMagBpmScale[ind])/100.0
            bpmy  = rmat[2,2]*quadd+rmat[2,3]*quadd*pylab.fabs(self.deltaSMagBpmScale[ind])/100.0
                        
            bpmxError.append(bpmx/quadd)
            bpmyError.append(bpmy/quadd)            
            
            print b,self.deltaSMagBpmScale[ind],bpmx/quadd, bpmy/quadd
        
        bpmxError = pylab.array(bpmxError)
        bpmyError = pylab.array(bpmyError)

        bpmxErrorStd = bpmxError.std()*100
        bpmyErrorStd = bpmyError.std()*100        

        pylab.figure(1)
        pylab.clf()

#        xhist = pylab.hist(bpmxError,20,(0.75,1.25),label='x : $\\sigma_x='+str(bpmxErrorStd)[0:5]+'$ \%',histtype='step')
#        yhist = pylab.hist(bpmyError,20,(0.75,1.25),label='y : $\\sigma_y='+str(bpmyErrorStd)[0:5]+'$ \%',histtype='step')

        xhist = pylab.hist(bpmxError,20,label='x : $\\sigma_x='+str(bpmxErrorStd)[0:5]+'$ \%',histtype='step')
        yhist = pylab.hist(bpmyError,20,label='y : $\\sigma_y='+str(bpmyErrorStd)[0:5]+'$ \%',histtype='step')

        pylab.legend()
        pylab.ylim(0,max([max(xhist[0]),max(yhist[0])])*1.4)

        pylab.xlabel('Quadrupole scale error ')
        pylab.ylabel('Entries')
