import numpy as np
import os
import shutil
import time
import pickle

from os.path import exists
from tempfile import mkstemp as _mkstemp
from shutil import move as _move
from os import remove as _remove
from os import close as _close

def DataType(filepath):
    filename = filepath.split('/')[-1][:-4]
    datatype = filename[-3:]
    return datatype

def GetLocalSettings():
    e = os.getenv('BACKGROUNDSETTINGS')
    if (type(e) == str):
        filepath = e
    else:
        filepath = '../LOCALSETTINGS.txt'
    a = GeneralString()
    a.Read(filepath)
    return a.data

def UpdateRunDir():
    #this should be run from the run dir!
    rundir = os.getcwd()+'/'
    a = GetLocalSettings()
    dd = a.data
    dd['rundir'] = rundir
    a.OverWrite(dd)

def Load(filename):
    a = Data()
    a.Read(filename)
    return a

class Data :
    """
    Read('../path/to/filename_typ.dat')

    Produces
    self.data - dictionary containing all data in file.
    self.datainfo - diciontary containing
    'type', 'noofdata','sampindex'

    NewFile('suffix',infolist=None)
    Creates new file

    Write(sample)
    sample is dictionary
    """
    
    def __init__(self) :
        self.data     = {}
        self.datainfo = {}
        self._testvar = 'detector1signal' #test pv to generate indices for data - should always be there
        self.__len__()

    def __len__(self):
        """
        return the length of the 0th dimension in the data - this is always the
        number of samples in each entry. Ie if a scan, it's the number of samples
        at each scan position. If a log, it's the number of samples in the log
        """
        try:
            self.shape = np.shape(self.data[self.data.keys()[0]])
            return self.shape[0]
        except (IndexError, KeyError, TypeError):
            self.shape = ()
            return 0

    def Clear(self) :
        self.data     = {}
        self.datainfo = {}

    def Read(self, filepath):
        self.filepath = filepath
        self.filename = filepath.split('/')[-1][:-4]
        datatype      = self.filename[-3:]
        self.datainfo['type'] = datatype
        
        if self.filename.startswith('20'):
            daydate   = int(self.filename[:8])
        else:
            daydate   = int(self.filename[:-7][-8:])
        print 'File Recorded on '+str(daydate)
        
        #read data
        #if self.datainfo['type'] == 'INI':
        #    print 'Initialisation Data Format'
        #    a = DataFormatIni()
        #    a.Read(filepath)
        #    self.data = a.data
        if (daydate > 20120000):
            print 'Data Format #3'
            a = DataFormat3()
            a.Read(filepath)
            self.data = a.data
            self.magdata = a.magdata
        else:
            print 'Unknown Data Format'

        #prepare some extra variables
        if self.data.has_key(self._testvar) == True:
            self.data['sampindex']      = np.arange(len(self.data[self._testvar]))
            self.datainfo['noofdata']   = len(self.data[self._testvar])
        else:
            l = self.data.keys()[0]
            if l == 'info':
                l = self.data.keys()[1]
                self.data['sampindex']      = np.arange(len(self.data[l]))
                self.datainfo['noofdata']   = len(self.data[l])
        if self.datainfo['type'] == 'INI':
            pass
        self.__len__() #update length and shape
    
    def NewFile(self,suffix,infolist=[]) :
        """Creates a new data file:
        
        ../dat/raw/YYYYMMDD_HHMM_suffix.dat
        
        Usage: NewFile(suffixstring,infolist(optional))
        
        """
        if suffix == 'INI':
            prefix = '../dat/ini/'
        else:
            prefix = '../dat/raw/'
        
        def GenerateName():
            ts = time.strftime('%Y%m%d_%H%M%S')
            self.filename     = ts+'_'+str(suffix)+'.dat'
            self.filepath     = prefix + self.filename
                
        GenerateName()
        while os.path.exists(self.filepath) == True:
            time.sleep(1.3)
            GenerateName()

        print 'Data.Data> opening new file ',self.filepath
        
        self.f            = open(self.filepath,'a',0)
        
        #Write supplied info value list
        infostring      = 'info> '+' '.join(map(str,infolist))+'\n'
        self.f.write(infostring)
    
    def WriteData(self,sample):
        """Writes data sample. NewFile must be called beforehand to create a file.
        
        Usage: WriteData(sample)
        
        Sample is a ordereddict type like 
        {datakey1:value(single value or array),datakey2:value...}
        
        """
        
        for datakey in sample:
            linestring = datakey+'> '+' '.join(map(str,np.array([sample[datakey]]).flatten()))+'\n'
            self.f.write(linestring)
        
    def CloseFile(self) :

        self.f.close()
        
class DataFormat3:
    def __init__(self) :
        self.data     = {}
        
    def Clear(self) :
        self.data     = {}
        
    def Read(self,filepath) :
        print 'Data.DataFormat3> reading ',filepath
        #read data
        f = open(filepath,'r')
        for line in f:
            dataline = line.rsplit('>')
            datakey  = dataline[0]
            data     = dataline[1].strip('\n').strip().split(' ')
            if self.data.has_key(datakey) == True:
                self.data[datakey].append(data)
            else :
                self.data[datakey] = []
                self.data[datakey].append(data)
        f.close()
        
        #turn data lists into arrays
        for parameter in self.data:
            if parameter == 'time':
                self.data[parameter] = np.array(self.data[parameter],dtype=str).flatten()
            elif (parameter != 'info') and (parameter != 'time'):
                self.data[parameter] = np.array(self.data[parameter],dtype=float)
                if len(np.shape(self.data[parameter])) > 1:
                    if np.shape(self.data[parameter])[1] == 1:
                        self.data[parameter] = self.data[parameter].flatten()
            elif parameter == 'info':
                self.data[parameter] = self.data[parameter][0]

        #extract single sample magnet data into different dict and del from data
        self.magdata = {}
        dellist      = []
        for key,val in self.data.iteritems():
            #if first letter is capital it's to do with magnets
            if np.str.isupper(key[0]):
                self.magdata[key] = float(val)
                dellist.append(key)
        #now delete from data
        for key in dellist:
            del self.data[key]

class DataFormatIni:
    def __init__(self):
        self.data = {}

    def Clear(self) :
        self.data     = {}
        
    def Read(self,filepath):
        print 'Data.DataFormatIni> reading ',filepath
        #read data
        f = open(filepath,'r')
        for line in f:
            dataline = line.rsplit('>')
            datakey  = dataline[0]
            data     = dataline[1].strip('\n').strip().split(' ')
            if self.data.has_key(datakey) == True:
                self.data[datakey].append(data)
            else :
                self.data[datakey] = []
                self.data[datakey].append(data)
        f.close()

        for p in self.data:
            if (p!='extlw:scan:axis') and (p!='info') and (p!='extlw:scan:y') and (p!='extlw:scan:fitmodel') and (p!='extlw:edm:fit'):
                self.data[p] = np.array(self.data[p],dtype=float)
                self.data[p] = self.data[p].flatten()

class General:
    """
    General format:
    
    key> value1 value2 etc

    If key already exists, append in new dimension.
    If there is only one value, remove list / array structure
    """

    def __init__(self):
        self.data = {}

    def Clear(self):
        self.data = {}

    def Read(self,filepath):
        self.filepath = filepath
        self.filename = filepath.split('/')[-1]
        print 'Data.General> reading ',filepath
        try:
            f = open(filepath,'r')
        except:
            print filepath,' does not exist'
            return
        
        lines = f.readlines()
        f.close()
        
        for line in lines:
            dataline  = line.split('>')
            datakey   = dataline[0]
            datavalue = dataline[1]
            datavalue = map(float,datavalue.strip('\n').strip().split(' '))
            self.data[datakey] = datavalue
            
        for key in self.data:
            if len(self.data[key]) == 1:
                self.data[key] = self.data[key][0]
            else:
                self.data[key] = np.array(self.data[key])

    def Write(self,filepath,datadict):
        f = open(filepath,'w')
        for key in datadict:
            if type(datadict[key]) != list:
                if type(datadict[key]) == np.ndarray:
                    datadict[key] = list(datadict[key])
                else:
                    datadict[key] = [datadict[key]]
        for key in datadict:
            linestring = str(key)+'> '+' '.join(map(str,datadict[key]))+'\n'
            f.write(linestring)
        f.close()
        print 'lwData.General> data written to ',filepath

    def WriteInOrder(self,filepath,datadict,keylist):
        f = open(filepath,'w')
        for key in datadict:
            if type(datadict[key]) != list:
                if type(datadict[key]) == np.ndarray:
                    datadict[key] = list(datadict[key])
                else:
                    datadict[key] = [datadict[key]]
        for key in keylist:
            linestring = str(key)+'> '+' '.join(map(str,datadict[key]))+'\n'
            f.write(linestring)
        f.close()
        print 'Data.General> data written to ',filepath
    
class GeneralString:
    def __init__(self):
        self.data     = {}
        self.filename = 'unknown.dat'
        self.filepath = 'unknown.dat'
        
    def Clear(self):
        self.data = {}

    def Read(self,filepath):
        self.filename = filepath.split('/')[-1]
        self.filepath = filepath
        f = open(filepath,'r')
        lines = f.readlines()
        f.close()

        for line in lines:
            key   = line.split('>')[0]
            value = line.split('>')[1].strip('\n').strip().strip("'")
            if self.data.has_key(key) != True:
                self.data[key] = value
            else:
                if type(self.data[key]) != list:
                    self.data[key] = list(self.data[key])
                    self.data[key] = self.data[key].append(value)
                else:
                    self.data[key] = self.data[key].append(value)

    def Write(self,datadict,filepath):
        self.filename = filepath.split('/')[-1]
        self.filepath = filepath
        f = open(filepath,'w')
        for key in datadict:
            stringtowrite = str(key)+'> '+str(datadict[key])+'\n'
            f.write(stringtowrite)
        f.close()

    def Append(self,datadict,filepath):
        self.fileobject = open(filepath,'a',0)
        for key in datadict:
            stringtowrite = '\n'+str(key)+"> '"+str(datadict[key])+"'\n"
            self.fileobject.write(stringtowrite)

    def Close(self):
        if hasattr(self,'fileobject') == True:
            self.fileobject.close()
            del self.fileobject

    def OverWrite(self,datadict):
        fh,abs_path = _mkstemp()
        new_file = open(abs_path,'w')
        for key in datadict:
            stringtowrite = str(key)+'> '+str(datadict[key])+'\n'
            new_file.write(stringtowrite)
        new_file.close()
        _close(fh)
        _remove(self.filepath)
        _move(abs_path,self.filepath)

class MultiData:
    def __init__(self):
        self.totaldata = {}

    def MultiRead(self,filelist):
        for file in filelist:
            print file
            a = Data()
            a.Read(file)
            for key in a.data:
                if self.totaldata.has_key(key) == True:
                    self.totaldata[key].extend(a.data[key])
                else :
                    self.totaldata[key] = []
                    self.totaldata[key].extend(a.data[key])
            del a

    def MultiReadSeparate(self,filelist):
        for file in fl:
            a = Data()
            a.Read(file)
            self.totaldata[a.filename] = a.data
            del a
