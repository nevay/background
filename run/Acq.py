#! /usr/bin/env python

import cothread
import cothread.catools as ca
import Data
import OrderedDict
import numpy as np
import time
import optparse

from OrderedDict import OrderedDict as od

class Acq :
    """Acquisition Class.
    
    Usage: 
    step 1) Acq(filesuffixstring,infolist(optional))
    step 2) Acq.AcquireN(integernumberofpulses)
    repeat step 2 to append more data to file
    step 3) Acq.CloseAcq()
    
    filesuffix *can be anything* but should be
    a three letter suffix describing the file purpose ie
    
    log - a piece of wood... 
    pos - positions - single sample of normal data
    tst - test
    ped - pedestal data for ADC

    INI - sample of EVERYTHING put in specail place to INItialise db
    
    Upon instantiation, a file is opened with name
    
    ../dat/raw/YYYYMMDD_HHMM_suffix.dat
    
    and remains open.
    
    Acquire pulses then close file to further writing.
    Data is written to file as it's acquired so if 
    process is killed, data will still exist.
    
    """
    
    def __init__(self,suffix,infolist=['NA']) :
        ca.caput('bg:progress',0,throw=False)
        self.d           = Data.Data()
        settings         = Data.GetLocalSettings()
        #triggers read of all pvs - should be latest published pv
        try:
            self.triggervar  = settings['triggervar']
        except KeyError:
            print "Please specify a 'triggervar' key in the LOCALSETTINGS.txt file"
            
        self.progressvar = 'bg:progress' #pv for % complete
        self.QuitEvent   = cothread.Event(auto_reset = False)
        self.nacquired   = 0
        self.infolist    = infolist
        self.suffix      = suffix

        self.d.NewFile(suffix,infolist)
        self.filename = self.d.filename
        self.filepath = self.d.filepath
        print 'New Data File '+self.filepath

        atf2magnets   = [
            'QM6R',  'QM7R',  'BS1X',  'ZV1X',  'QS1X',  'QF1X',   'ZH1X',    'ZX1X',
            'ZV2X',  'QD2X',  'QF3X',  'ZH1X',  'ZV3X',  'QF4X',   'QD5X',    'ZV4X',
            'ZX2X',  'ZV5X',  'QF6X',  'QS2X',  'BH3X',  'ZV3X',   'QF7X',    'ZH3X',
            'QD8X',  'ZV6X',  'QF9X',  'ZH4X',  'ZV7X',  'QK1X',   'QD10X',   'ZH5X',
            'QF11X', 'ZV8X',  'QK2X',  'QD12X', 'ZH6X',  'QD14X',  'ZH7X',    'QF15X',
            'ZV9X',  'QK3X',  'QD16X', 'ZH8X',  'QF17X', 'QK4X',   'QD18X',   'ZH9X',
            'QF19X', 'ZV11X', 'QD20X', 'ZH10X', 'QF21X', 'QM16FF', 'ZH1FF',   'ZV1FF',
            'QM15FF','QM14FF','QM13FF','QM12FF','QM11FF','QD10BFF','QD10AFF', 'QF9BFF',
            'SF6FF', 'QF9AFF','QD8FF', 'QF7FF', 'B5FF',  'QD6FF',  'QF5BFF',  'SF5FF',
            'QF5AFF','QD4BFF','SD4FF', 'QD4AFF','B2FF',  'QF3FF',  'B1FF',    'QD2BFF',
            'QD2AFF','SF1FF', 'QF1FF', 'SD0FF', 'QD0FF', 'BDUMP'
        ]
        magpvs            = [x+':currentRead' for x in atf2magnets]
        #bbamagnets        = [
        #    'QM16FF','QM15FF','QM14FF','QM13FF','QM12FF','QM11FF', 'QD10BFF', 'QD10AFF',
        #    'QF9BFF','SF6FF','QF9AFF','QD8FF','QF7FF','QD6FF','QF5BFF','QFABFF',
        #    'QD4BFF','QD4AFF','QF3FF','QD2BFF','QD2AFF','QF1FF','QD0FF'
        #]
        bbamagnets =        [
            'QD10X', 'QF11X',  'QD12X',  'QD16X', 'QF17X',  'QD18X',  'QF19X', 'QD20X',
            'QF21X', 'IPT1',   'IPT2',   'IPT3',  'IPT4',   'QM16FF', 'QM15FF','QM14FF',
            'FB2FF', 'QM13FF', 'QM12FF', 'QM11FF','QD10BFF','QD10AFF','QF9BFF','SF6FF',
            'QF9AFF','QD8FF',  'QF7FF',  'QD6FF', 'QF5BFF', 'SF5FF',  'QF5AFF','QD4BFF',
            'SD4FF', 'QD4AFF', 'QF3FF',  'QD2BFF','QD2AFF', 'SF1FF',  'QF1FF', 'SD0FF',
            'QD0FF', 'PREIP',  'IPA',    'IPB',   'M-PIP',  'QD10X',  'QF11X', 'QD12X',
            'QD16X', 'QF17X',  'QD18X',  'QF19X', 'QD20X',  'QF21X',  'IPT1',  'IPT2',
            'IPT3',  'IPT4',   'QM16FF', 'QM15FF','QM14FF', 'FB2FF',  'QM13FF','QM12FF',
            'QM11FF','QD10BFF','QD10AFF','QF9BFF','SF6FF',  'QF9AFF', 'QD8FF', 'QF7FF',
            'QD6FF', 'QF5BFF', 'SF5FF',  'QF5AFF','QD4BFF', 'SD4FF',  'QD4AFF','QF3FF',
            'QD2BFF','QD2AFF', 'SF1FF',  'QF1FF', 'SD0FF',  'QD0FF',  'PREIP', 'IPA',
            'IPB',   'M-PIP'
        ]

        magmovs           = {
            'M-PIP':   'c1:qmov:m4',
            'QD0FF':   'c1:qmov:m28',
            'QD10AFF': 'c1:qmov:m9',
            'QD10BFF': 'c1:qmov:m8',
            'QD2AFF':  'c1:qmov:m24',	
            'QD2BFF':  'c1:qmov:m23',
            'QD4AFF':  'c1:qmov:m21',
            'QD4BFF':  'c1:qmov:m19',
            'QD6FF':   'c1:qmov:m15',
            'QD8FF':   'c1:qmov:m13',
            'QF1FF':   'c1:qmov:m26',
            'QF3FF':   'c1:qmov:m22',
            'QF5AFF':  'c1:qmov:m18',
            'QF5BFF':  'c1:qmov:m16',
            'QF7FF':   'c1:qmov:m14',
            'QF9AFF':  'c1:qmov:m12',
            'QF9BFF':  'c1:qmov:m10',
            'QM11FF':  'c1:qmov:m7',
            'QM12FF':  'c1:qmov:m6',
            'QM13FF':  'c1:qmov:m5',
            'QM14FF':  'c1:qmov:m3',
            'QM15FF':  'c1:qmov:m2',
            'QM16FF':  'c1:qmov:m1',
            'SD0FF':   'c1:qmov:m27',
            'SD4FF':   'c1:qmov:m20',
            'SF1FF':  'c1:qmov:m25',
            'SF5FF':  'c1:qmov:m17',
            'SF6FF':  'c1:qmov:m11'
        }
        magmovsexpanded = {}
        for k,v in magmovs.iteritems():
            magmovsexpanded[k+'xpos'] = v+':x'
            magmovsexpanded[k+'ypos'] = v+':y'
            magmovsexpanded[k+'tilt'] = v+':tilt'

        self.magmovkeys   = magmovsexpanded
        bbaxpvs           = [name+'x:bbaOffset' for name in bbamagnets]
        bbaypvs           = [name+'y:bbaOffset' for name in bbamagnets]
        self.bbaxkeys     = dict(zip(bbaxpvs,bbaxpvs))
        self.bbaykeys     = dict(zip(bbaypvs,bbaypvs))
        self.magkeys      = dict(zip(atf2magnets,magpvs))
                                 
        if self.suffix == 'ped':
            self.datakeys = od({
                'adcdataraw'     :'bg:adcdata:raw'
                })

        elif self.suffix == 'INI':
            self.datakeys = od({
                'bg:detector1:signal'    :'bg:detector1:signal',
                'bg:detector1:background':'bg:detector1:background',
                'bg:detector1:adcraw'    :'bg:detector1:adcraw',
                'bg:detector2:signal'    :'bg:detector2:signal',
                'bg:detector2:background':'bg:detector2:background',
                'bg:detector2:adcraw'    :'bg:detector2:adcraw',
                'bg:adcdata:signal'      :'bg:adcdata:signal',
                'bg:adcdata:raw'         :'bg:adcdata:raw',
                'bg:adcdata:background'  :'bg:adcdata:background',
                'refc1amp'               :'REFC1:amp'
                })
        else:
            # name in data file : EPICS PV
            self.datakeys = od({
                'detector1signal'    :'bg:detector1:signal',
                'detector1raw'       :'bg:detector1:adcraw',
                'detector1background':'bg:detector1:background',
                'detector2signal'    :'bg:detector2:signal',
                'detector2raw'       :'bg:detector2:adcraw',
                'detector2background':'bg:detector2:background',
                'adcdatasignal'      :'bg:adcdata:signal',
                'adcdataraw'         :'bg:adcdata:raw',
                'adcdatabackground'  :'bg:adcdata:background',
                'detector1voltage'   :'EXT:PMT_A:ACT_VOL_READ',
                'detector2voltage'   :'EXT:PMT_B:ACT_VOL_READ',
                'refc1amp'           :'REFC1:amp',
                'cbpmx'              :'cbpm:xpos',
                'cbpmy'              :'cbpm:ypos',
                'atfext'             :'BIM:EXT:nparticles',
                'atfip'              :'BIM:IP:nparticles',
                'refc1phase'         :'REFC1:pha',
                'atf2xpos'           :'atf2:xpos',
                'atf2ypos'           :'atf2:ypos'
                })
        self.AcquireMags()
        
        self.ps = []
        self.ks = []
        for key,pv in self.datakeys.iteritems():
            self.ps.append(pv)
            self.ks.append(key)                

    def Reset(self) :
        self.QuitEvent.Signal()
        if hasattr(self,'s'):
            self.s.close()
        
    def Callback(self,value) :
        print 'Acq> callback> ',self.npulse

        self.npulse = self.npulse + 1
        self.nacquired = self.nacquired + 1
        
        d = (ca.caget(self.ps))
        sample = dict(zip(self.ks,d))

        self.d.WriteData(sample)
        
        perc = (float(self.npulse)/self.npulsemax)*100
        ca.caput(self.progressvar,perc)
        
        if self.npulse==self.npulsemax:
            self.Reset()
    
    def AcquireN(self,npulsemax) :
        """Acquire a number of pulses
        
        Usage AcquireN(integer)
        
        Data samples consisting of 'datakeys' in init method
        are saved upon a trigger from self.triggervar, also 
        defined in init.
        
        File remains open
        
        """
        
        print 'Acquiring ',npulsemax,' pulses'
        ca.caput(self.progressvar,0,throw=False)
        self.npulse = 0
        self.npulsemax = npulsemax
        self.QuitEvent.Reset()
        
        self.s = ca.camonitor(self.triggervar,self.Callback)
        
        self.QuitEvent.Wait()
           
        print self.npulse,' Pulses Acquired'

    def AcquireTemp(self):
        sample={}
        for key, pv in self.datakeys.iteritems():
            sample[key] = ca.caget(pv)
        self.d.WriteData(sample)

    def AcquireMags(self):
        print 'Acq> Acquiring Magnet Values'

        #now for some fiddly bits - magnet don't always seem to connect
        #then script barfs and you have lots of empty data files!
        a = ca.caget(self.progressvar) #get a generic float type
        a.ok = False #set .ok to false huzzah
        ntrys = 0
        while a.ok != True:
            a = ca.caget('QM6R:currentRead',timeout=1,throw=False)
            time.sleep(0.5)
            print 'Magnet Acquisition: ',a.ok
            ntrys += 1
            if ntrys > 10:
                print 'Magnet Acquisition failed!'
                return

        print 'Proceeding...'
        
        sample={}
        a = ca.caget(self.progressvar) #get a generic float type
        a.ok=False #set .ok to false huzzah
        res = [a]
        
        while res[0].ok == False:
            res = ca.caget(self.magkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.magkeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        
        while res[0].ok == False:
            res = ca.caget(self.bbaxkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.bbaxkeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        
        while res[0].ok == False:
            res = ca.caget(self.bbaykeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.bbaykeys.keys(),res))
        sample.update(res_dict)
        res = [a]
        
        while res[0].ok == False:
            #keys are the name in data
            #values are the pvs of the variables
            res = ca.caget(self.magkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.magkeys.keys(),res))
        sample.update(res_dict)
        res = [a]

        while res[0].ok == False:
            res = ca.caget(self.magmovkeys.values(),timeout=2.0,throw=False)
        res_dict = dict(zip(self.magmovkeys.keys(),res))
        sample.update(res_dict)
          
        self.d.WriteData(sample)

    def SingleAcquireVars(self):
        print 'Acq> Acquiring Once Only Vars'
        
    def CloseAcq(self) :
        """CloseAcq() - closes the data file - no further writing 
        possible to the data file associated with this instance of lwAcq.
        
        """
        
        self.d.CloseFile()
        print '\nData written to ',self.d.filepath

    def ContinueInNewFile(self):
        self.d.CloseFile()
        self.d.NewFile(self.suffix,self.infolist)
        self.filename = self.d.filename
        self.filepath = self.d.filepath
        print 'New Data File '+self.filepath

    
class Logger :
    """Logger Class
    
    Usage: 
    Logger()
    Logger.Log(integernumberofpulses)
    
    Data saved to ../dat/raw/YYMMDD_HHMM_log.dat.  
    
    """
        
    def __init__(self) :
        pass
        
    def Log(self,npulse) :
        
        print 'Logging ',npulse,' pulses\n'
        a   = Acq('log',['log',npulse])
        his = History()
        his.RecordLog(npulse,a.filename)
        
        try:
            a.AcquireN(npulse)
            a.CloseAcq()
        except KeyboardInterrupt:
            a.Reset()
            print '\nLogging Quit'

class Positions :
    def __init__(self) :
        pass

    def Save(self) :
        print 'Saving positions\n'
        a   = Acq('pos',['positions'])
        his = History()
        his.RecordPos(a.filename)
        
        a.AcquireN(1)
        a.CloseAcq()

class History:
    def __init__(self):
        self.hf = open('../dat/his/History.dat','a',0)

    def RecordMove(self,axis,currentpos,targetpos):
        self.f      = open('../dat/his/MoveHistory.dat','a',0)
        #self.f      = open('/Network/Servers/atf-xserve2.atf-local/Volumes/vtrak0/userhome/atfopr/extlw/dat/his/lwMoveHistory.dat','a',0)

        t           = time.strftime('%Y%m%d_%H%M%S')
        linetowrite = 'Move> '+t+' axis='+str(axis)+' cpos='+str(currentpos)+' tpos='+str(targetpos)+'\n'
        self.f.write(linetowrite)
        self.f.close()

    def RecordScan(self,axis,npulse,start,stop,nstep,filename,totaltime=-1.0):
        linetowrite = '   * Scan> '+str(filename)+' axis='+str(axis)+', npulse='+str(npulse)+', start='+str(start)+', stop='+str(stop)+', nstep='+str(nstep)+', totaltime='+str(totaltime)+'\n'
        self.hf.write(linetowrite)
        self.hf.close()

    def RecordLog(self,npulse,filename):
        linetowrite = '   * Log> '+str(filename)+' npulse='+str(npulse)+'\n'
        self.hf.write(linetowrite)
        self.hf.close()

    def RecordPos(self,filename):
        linetowrite = '   * Pos> Positions saved '+str(filename)+'\n'
        self.hf.write(linetowrite)
        self.hf.close()
    

def main() :
    usage = ''
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--log',action='store_true',default=False)  #log n pulses
    parser.add_option('-b','--pos',action='store_true',default=False)  #log 1 pulse for positions
    parser.add_option('-c','--l500',action='store_true',default=False) #log 500 pulses for positions
    parser.add_option('-d','--l100',action='store_true',default=False) #log 100 pulses for positions

    options,args = parser.parse_args()
    
    if options.log :
        l = Logger()
        npulse = ca.caget('bg:edm:logn')
        l.Log(npulse)

    if options.pos :
        p = Positions()
        p.Save()

    if options.l500:
        l = Logger()
        l.Log(500)

    if options.l100:
        l = Logger()
        l.Log(100)

if __name__ == "__main__":
    main()
