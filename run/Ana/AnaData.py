import General
import Data
import numpy as np
import os
import Parameters
import sys
import string
import Orbit

flatten = np.ndarray.flatten

def AnaDir(existinganadir,filename):
    #check if there's a folder for current file
    #if not make one
    #return this dir
    filenames = filename.strip('.dat')
    idealdir = existinganadir+filenames+'/'
    if os.path.exists(idealdir):
        anadir = idealdir
    else:
        os.mkdir(idealdir)
        anadir = idealdir
    return anadir

def DualAnaDir(hfilename,vfilename):
    #create a new analysis directory that's unique
    #as it's for the combined analysis of two scans
    localsettings = Data.GetLocalSettings()
    hfilenames = hfilename.strip('.dat')
    vfilenames = vfilename.strip('.dat')
    
    idealdir = localsettings['anadir']+hfilenames+'_'+vfilenames+'/'
    if os.path.exists(idealdir):
        anadir = idealdir
    else:
        os.mkdir(idealdir)
        anadir = idealdir
    return anadir

def TrimDodgySamples(data,datainfo):
    print 'lwAna.Analysis> \tChecking if file is complete'

    # check if data file is complete
    if ((datainfo['noofstep']*datainfo['noofsamp']) != datainfo['noofdata']):
        # n - number
        # i or d or c - pertaining to info or data or calculated
        nistep = datainfo['noofstep']
        nisamp = datainfo['noofsamp']
        nidata = nistep*nisamp
        nddata = datainfo['noofdata']
        print 'lwAna.Analysis> \t',
        print 'WARNING: Number of data points does not match included file info'
        print 'lwAna.Analysis> \tInfo total number of samples is ',nidata
        print 'lwAna.Analysis> \tRecorded number of samples is   ',nddata
        
        # number of steps recorded in data
        ndstep = float(nddata)/float(nisamp)
        
        # number of complete steps recorded in data
        ndcomp = np.floor(ndstep)
        
        # number of data samples to use then
        ncdata = ndcomp*nisamp
            
        for parameter in data:
            if ((parameter == 'cbpmx') or (parameter == 'cbpmy')):
                data[parameter] = data[parameter][:ncdata,:]
            else:
                data[parameter] = data[parameter][:ncdata]
        datainfo['noofdataraw'] = datainfo['noofdata']
        datainfo['noofstepraw'] = datainfo['noofstep']
        
        datainfo['noofdata']    = ncdata
        datainfo['noofstep']    = ndcomp
    else:
        print 'lwAna.Analysis> \tFile complete'
        
    return data,datainfo

def DataReshapeSamples(datadict,datainfo):
    print 'lwAna.Analysis> \tReshaping raw data'
    # reshape raw data from file into nice arrays
    tdata = {}
    for key, value in datadict.iteritems():
        if len(np.shape(value)) == 1:
            tdata[key] = np.reshape(datadict[key],(datainfo['noofstep'],datainfo['noofsamp']))
        else:
            tdata[key] = np.reshape(datadict[key],(datainfo['noofstep'],datainfo['noofsamp'],np.shape(value)[1]))
    return tdata

def DataAvg(filtereddata):
    print 'lwAna.Analysis> \tPreparing averaged data'
    # note masked arrays require special methods
    # custom std and mean take care of this and return to np array type
    averaged         = {}
    averagedstd      = {}
    for key, value in filtereddata.iteritems():
        sys.stdout.write(".")
        sys.stdout.flush()
        averaged[key]    = General.Mean(value,axis=1)
        averagedstd[key] = General.StdOfMean(value)
    sys.stdout.write("\r")
    return averaged,averagedstd

def Background(x,y,frac=5):
    """
    Background(x,y,frac=5)
    x - xdata (1d np array)
    y - ydata (1d np array)
    frac - percentage of data to use
    
    Averages first and last 'frac'% of data and calculates straight line between
    these points to give a background line.  Background subtracted data
    is calculated.
    
    return ybg, ybgs,m,xb1,yb1
    ybg  - background data corresponding to x
    ybgs - y background subtracted
    xb1  - x point on background line
    yb1  - y point on background line
    """
    if len(x) != len(y):
        print 'len(x) != len(y)'
        return
    originalshape = np.shape(x)
    x = flatten(x)
    y = flatten(y)
    fraction = frac #percentage of data to use at each end
    bgind1   = int(len(y)*(fraction/100.))
    bgind2   = len(y)-bgind1
    yb1      = np.mean(y[:bgind1])
    yb2      = np.mean(y[bgind2:])
    xb1      = np.mean(x[:bgind1])
    xb2      = np.mean(x[bgind2:])
    m        = (yb2-yb1)/(xb2-xb1)
    ybg      = m*(x-xb1) + yb1 
    ybgs     = y - ybg 
    ybg      = np.reshape(ybg,originalshape)
    ybgs     = np.reshape(ybgs,originalshape)
    return ybg,ybgs,m,xb1,yb1

class AnaData:
    def __init__(self,filename):
        #main data structures
        self.raw          = {}
        self.filtered     = {}
        self.filteredflat = {}
        self.averaged     = {}
        self.averagedstd  = {}
        self.keys         = []
        self.r            = self.raw
        self.f            = self.filtered
        self.a            = self.averaged
        self.astd         = self.averagedstd
        self.ff           = self.filteredflat
        self.filterhistory = []

        # get directories
        localsettings  = Data.GetLocalSettings()
        self.rundir    = localsettings['rundir']
        self.anadir    = localsettings['anadir']
        self.datdir    = localsettings['datdir']
        self.printmode = int(localsettings['printmode'][0])

        # set directories
        self.anadir   = AnaDir(self.anadir,filename)
        
        # load raw data - fo = fileobject
        if os.path.exists(filename):
            fo = Data.Load(filename)
        else:
            fo = Data.Load(self.datdir+filename)
        self.filename            = fo.filename
        self.filepath            = fo.filepath
        self.raw                 = fo.data
        self.datainfo            = fo.datainfo
        self.datainfo['inforaw'] = self.raw['info']
        del self.raw['info']
        self.parameters['nbpms'] = np.shape(self.raw['cbpmx'])[1]
        self.nbpms               = np.shape(self.raw['cbpmx'])[1]
        if self.raw.has_key('atf2xpos'):
            self.nbpmsatf2       = np.shape(self.raw['atf2xpos'])[1]
            self.parameters['nbpmsatf2'] = np.shape(self.raw['atf2xpos'])[1]
        if hasattr(fo,'magdata'):
            self.magdata = fo.magdata

        self.shape = fo.shape
        self._datafile = fo

        # Data type specific operations
        # set datainfo['noofstep'] and datainfo['noofsamp'] in each case
        if self.datainfo['type'] == 'log':
            print 'Log file type'
            self.datainfo['noofstep'] = int(float(self.datainfo['inforaw'][-1]))
            self.datainfo['noofsamp'] = 1
            self.raw,self.datainfo    = TrimDodgySamples(self.raw,self.datainfo)
            self._MakeNormalisedData()

        if self.datainfo['type'] == 'pos':
            print 'lwAna.Analysis> \tPosition file type'
            self.datainfo['noofstep'] = 1
            self.datainfo['noofsamp'] = 1

        if self.datainfo['type'] == 'tst':
            print 'lwAna.Analysis> \tTest file type'
            self.datainfo['noofstep'] = len(self.data['refc1amp'])
            #self.datainfo['noofstep'] = int(float(self.data['info'][-4]))
            self.datainfo['noofsamp'] = 1

        if self.datainfo['type'] == 'ped':
            print 'lwAna.Analysis> \tPedestal file type'
            self.datainfo['noofstep'] = len(self.data['adcdata'])
            self.datainfo['noofsamp'] = 1
        
        # other general stuff
        # self.raw at this point is only full length data
        # other stuff is in self.datainfo
        # setup default xaxis and labels
        self.x                       = self.raw['sampindex']
        self.y                       = np.zeros_like(self.x)
        self.xlabel                  = 'Sample Number'
        self.ylabel                  = ' '
        self.filter                  = np.zeros((self.datainfo['noofstep'],self.datainfo['noofsamp']),dtype=bool)
        self.parameters['datakeys']  = np.sort(self.raw.keys())
        self.existingfiles           = os.listdir(self.anadir)
        
        self.raw = DataReshapeSamples(self.raw,self.datainfo)
        self.r   = self.raw
        self.FilterApply() #Data averaging done each time filter is updated
        self._UpdateKeys()

    def __len__(self):
        return self._datafile.__len__()
    
    def __call__(self,datatype=None):
        if datatype == None:
            return self.raw
        elif ( (datatype != None) & (hasattr(self,datatype) == False) ):
            print 'Invalid datatype'
            print "'raw', 'averaged', 'filtered'"
        else:
            return getattr(self,datatype)

    def __repr__(self):
        sortedkeys = np.sort(self.raw.keys())
        s = 'Variable Name       Shape\n'
        for key in sortedkeys:
            ss = key
            while len(ss) < 20:
                ss = ss + ' '
            s = s +ss + str(np.shape(self.raw[key]))+'\n'
        s = s.strip('\n')
        return s

    def __getitem__(self,inputvals):
        if type(inputvals) == tuple:
            variablename = inputvals[0]
            datatype     = inputvals[1]
        else:
            variablename = inputvals
            datatype     = 'None'
        if self.raw.has_key(variablename) == False:
            print 'Invalid Data Key'
            print 'See self.keys for list of keys'
        elif ( (datatype != 'None') & ((hasattr(self,datatype) == False)) ):
            print hasattr(self,datatype)
            print 'Invalid datatype'
            print "'raw', 'averaged', 'filtered'"
        elif datatype == 'None':
            return self.raw[variablename]
        else:
            d = getattr(self,datatype)
            return d[variablename]

    def _UpdateKeys(self):
        self.keys = self.averaged.keys()
        self.keys.sort()
                
    def _MakeNormalisedData(self):
        print 'lwAna.Analysis> \tPreparing normalised data'
        # only use for 'lws' file type - dependencies
        
        # add calculated data
        refc1amp        = self.raw['refc1amp']
        detector1signal = self.raw['detector1signal']
        detector2signal = self.raw['detector2signal']
        
        # replace 0 values for division
        if np.any(detector1signal==0):
            detector1signal = General.ReplaceZero(detector1signal,1e-9)
        if np.any(detector2signal==0):
            detector2signal = General.ReplaceZero(detector2signal,1e-9)

        # prepare charge in real units
        refc1ampine = refc1amp/2581.69
        
        # charge normalise
        detector1signalcn  = detector1signal/refc1amp
        detector2signalcn  = detector2signal/refc1amp
        detector1signalcne = detector1signal/refc1ampine
        detector2signalcne = detector1signal/refc1ampine
        
        # publish to data
        self.raw['detector1signalcn']  = detector1signalcn
        self.raw['detector2signalcn']  = detector2signalcn
        self.raw['detector1signalcne'] = detector1signalcne
        self.raw['detector2signalcne'] = detector2signalcne
        self.raw['refc1ampine']        = refc1ampine

        # publish datainfo
        self.parameterinfo['detector1signalcn']  = ['Charge Normalised Detector 1 (A.U.)',0.01]
        self.parameterinfo['detector2signalcn']  = ['Charge Normalised Detector 2 (A.U.)',0.01]
        self.parameterinfo['detector1signalcne'] = ['Charge Normalised Detector 1 (x e$^{-}$)',0.01]
        self.parameterinfo['detector2signalcne'] = ['Charge Normalised Detector 2 (x e$^{-}$)',0.01]
        self.parameterinfo['refc1ampine']        = [r'Charge ($\\times$~10$^{10}$~e$^{-}$)',0.001]
            
        # update keys
        self._UpdateKeys()

    def FilterAboveLimit(self,key,upperlimit=None):
        """
        FilterAboveLimit(key,upperlimit)

        key        - string
        upperlimit - float

        Filter all data recorded based on variable 'key' when it is above
        'upperlimit'.

        ie FilterAboveLimit('refc1amp',2500)

        """
        if self.raw.has_key(key) == True:
            filter = self.raw[key]>upperlimit #true = masked
            nfiltered = (filter == True).sum()
            print 'lwAna.Analysis> \t'+str(nfiltered)+' data points filtered out'
            self.filter = self.filter | filter
            self.FilterApply()
            filterstring = 'FilterAboveLimit '+str(key)+' '+str(upperlimit)
            self.filterhistory.append(filterstring)
        elif self.raw.has_key(key) != True:
            print 'Invalid Data Key'
            print 'See self.keys for list of keys'
            
    def FilterBelowLimit(self,key,lowerlimit=None):
        """
        FilterBelowLimit(key,lowerlimit)

        key        - string
        lowerlimit - float

        Filter all data recorded based on variable 'key' when it is below
        'lowerlimit'.

        ie FilterBelowLimit('refc1amp',500)

        """
        if self.raw.has_key(key) == True:
            filter = self.raw[key]<lowerlimit #true = masked
            nfiltered = (filter == True).sum()
            print 'lwAna.Analysis> \t'+str(nfiltered)+' data points filtered out'
            self.filter= self.filter | filter
            self.FilterApply()
            filterstring = 'FilterBelowLimit '+str(key)+' '+str(lowerlimit)
            self.filterhistory.append(filterstring)
        elif self.raw.has_key(key) != True:
            print 'Invalid Data Key'
            print 'See self.keys for list of keys'
            
    def FilterInRange(self,key,lowerlimit=None,upperlimit=None):
        """
        FilterInRange(key,lowerlimit,upperlimit)

        key        - string
        lowerlimit - float
        upperlimit - float

        Filter all data recorded based on variable 'key' when it is below
        'lowerlimit' and above 'upperlimit'.

        ie FilterBelowLimit('refc1amp',500)

        """
        if self.raw.has_key(key) == True:
            filter = (self.raw[key]>upperlimit) | (self.raw[key]<lowerlimit) #true = masked
            nfiltered = (filter == True).sum()
            print 'lwAna.Analysis> \t'+str(nfiltered)+' data points filtered out'
            self.filter = self.filter | filter
            self.FilterApply()
            filterstring = 'FilterInRange '+str(key)+' '+str(lowerlimit)+' '+str(upperlimit)
            self.filterhistory.append(filterstring)
        elif self.raw.has_key(key) != True:
            print 'Invalid Data Key'
            print 'See self.keys for list of keys'

    def FilterOutsideNSigma(self,key,sigma=5.0):
        """
        FilterOutsideNSigma(key,sigma=5.0)

        key        - string
        sigma      - float

        Filter all data recorded based on variable 'key' when it is outside
        'sigma' times the standard deviation of 'key'.  

        ie FilterOutsideNSigma('refc1amp')
           FilterOutisdeNSigma('refc1amp',3)

        Standard Deviation is calculated as:
        sqrt( (1/N) * Sum(x-mean(x)) )
        using the raw data for 'key'

        """
        
        if self.raw.has_key(key) == True:
            fs     = sigma*np.std(self.raw[key])
            avg    = np.average(self.raw[key])
            lowerlimit = avg-fs
            upperlimit = avg+fs

            filter = (self.raw[key]>upperlimit) | (self.raw[key]<lowerlimit) #true = masked
            nfiltered = (filter == True).sum()
            print 'lwAna.Analysis> \t'+str(nfiltered)+' data points filtered out'
            self.filter = self.filter | filter
            self.FilterApply()
            filterstring = 'FilterOutsideNSigma '+str(key)+' '+str(sigma)
            self.filterhistory.append(filterstring)
        elif self.raw.has_key(key) != True:
            print 'Invalid Data Key'
            print 'See self.keys for list of keys'
        
    def FilterApply(self):
        """
        FilterApply()

        Applies the self.filter boolean list to the data.  This list has the same
        dimenstionality / structure as the data organised into pulses & samples.  

        Each boolean value represents a machine pulse and whether it is to be filtered
        or not.  If it is, FilterApply will mask it in the filtered copy of the data.  
        The boolean array will be be automatically extend for higher dimension data ie
        cbpm data.

        This is run automatically after each filter.

        """
        print 'Ana.Analysis> \tApplying filter'
        for key, value in self.raw.iteritems() :
            if np.ndim(value) == 2:
                self.filtered[key] = np.ma.array(self.raw[key],mask=self.filter)
            elif np.ndim(value) == 3:
                i,j,k  = np.shape(value)
                nreps  = k
                filtertemp = np.repeat(self.filter,nreps,axis=1).reshape(i,j,k)
                self.filtered[key] = np.ma.array(self.raw[key],mask=filtertemp)
        self.averaged,self.averagedstd = DataAvg(self.filtered)
        self.a    = self.averaged
        self.astd = self.averagedstd
        for key, value in self.filtered.iteritems():
            self.filteredflat[key] = self.filtered[key].compressed()
        self.ff = self.filteredflat

    def FilterReset(self):
        self.filter[:] = False
        self.FilterApply()
        filterstring = 'FilterReset'
        self.filterhistory.append(filterstring)
    
    def X(self,key,datatype='raw',bpmaxis=None) :
        """
        X(key,datatype='raw',bpmaxis=None)
        key      - data key ie 'chaver'
        datatype - 'raw', 'filtered', 'averaged', 'filteredflat' (optional - raw default)
        datatype - 'r', 'f', 'a', 'ff'                           (alternative usage)
        bpmaxis  - 'x', 'y'                      

        sets self.x, self.xstd, self.xlabel

        Use to assign data for analysis. Can use a data key or can use a 
        bpm name - ie 'QM14FF'.  If using a bpm name, you can optionally
        specify an axis.  If left unspecified, it will be x as this is the
        X method.  
        """
        
        #test key and dataype first
        #check if bpm specific
        #else normal extraction
        problem = False
        if hasattr(self,datatype) == False:
            print 'Invalid Data Type'
            print "Choose from 'raw' 'filtered' 'averaged'"
            problem = True
        elif (self.raw.has_key(key) == False) and (Parameters.bpminfo.has_key(key) == False):
            print 'Invalid Key'
            print 'See self.keys for list of keys'
            problem = True
        elif (Parameters.bpminfo.has_key(key) == True):
            # specific bpm
            if bpmaxis != None:
                bpmkey      = 'cbpm'+bpmaxis
                xlabel      = key+' '+bpmaxis+' Position ($\mu$m)'
            else:
                bpmkey      = 'cbpmx'
                xlabel       = key+' Position ($\mu$m)'
            d    = getattr(self,datatype)
            x    = d[bpmkey][:,:,Parameters.bpminfo[key]]
            xstd = np.ones_like(x)*Parameters.parameterinfo[bpmkey][1]
            #replace with full resolution array later
            #raw -> std = resolution
            #avg -> std = std
            #fil -> std = resolution

        else:
            d = getattr(self,datatype)
            x = d[key]
            if ( (datatype == 'averaged') | (datatype == 'a') ):
                xstd = self.averagedstd[key]
                # to deal with if the average is 0 because of filtering!
                if hasattr(self,'avg_filter') == True:
                    newmask         = x == 0
                    self.avg_filter = self.avg_filter | newmask
                else:
                    self.avg_filter = x == 0
                x    = x[np.logical_not(self.avg_filter)]
                xstd = xstd[np.logical_not(self.avg_filter)]
            else:
                xstd = np.ones_like(x)*self.parameterinfo[key][1]
            xlabel = self.parameterinfo[key][0]
            xstd[xstd<self.parameterinfo[key][1]] = self.parameterinfo[key][1]
        
        if problem == False:
            self.x       = x
            self.xstd    = xstd
            self.xlabel  = xlabel

    def Y(self,key,datatype='raw',bpmaxis=None) :
        """
        Y(key,datatype='raw',bpmaxis=None)
        key      - data key ie 'chaver'
        datatype - 'raw', 'filtered', 'averaged' (optional - raw default)
        datatype - 'r', 'f', 'a'                 (alternative usage)
        bpmaxis  - 'x', 'y'                      

        sets self.y, self.ystd, self.ylabel

        Use to assign data for analysis. Can use a data key or can use a 
        bpm name - ie 'QM14FF'.  If using a bpm name, you can optionally
        specify an axis.  If left unspecified, it will be y as this is the
        Y method.  

        """
        problem = False
        #test key and dataype first
        #check if bpm specific
        #else normal extraction
        if hasattr(self,datatype) == False:
            print 'Invalid Data Type'
            print "Choose from 'raw' 'filtered' 'averaged'"
            problem = True
        elif (self.raw.has_key(key) == False) and (Parameters.bpminfo.has_key(key) == False):
            print 'Invalid Key'
            print 'See self.keys for list of keys'
            problem = True
        elif (Parameters.bpminfo.has_key(key) == True):
            # specific bpm
            if bpmaxis != None:
                bpmkey      = 'cbpm'+bpmaxis
                ylabel      = key+' '+bpmaxis+' Position ($\mu$m)'
            else:
                bpmkey      = 'cbpmy'
                ylabel       = key+' Position ($\mu$m)'

            d    = getattr(self,datatype)
            y    = d[bpmkey][...,Parameters.bpminfo[key]]
            ystd = np.ones_like(y)*Parameters.parameterinfo[bpmkey][1]
            #replace with full resolution array later
            #raw -> std = resolution
            #avg -> std = std
            #fil -> std = resolution

        else:
            d = getattr(self,datatype)
            y = d[key]
            if ( (datatype == 'averaged') | (datatype == 'a') ):
                ystd = self.averagedstd[key]
                # to deal with if the average is 0 because of filtering!
                if hasattr(self,'avg_filter') == True:
                    newmask         = y == 0
                    self.avg_filter = self.avg_filter | newmask
                else:
                    self.avg_filter = y == 0
                y    = y[np.logical_not(self.avg_filter)]
                ystd = ystd[np.logical_not(self.avg_filter)]
            else:
                ystd = np.ones_like(y)*self.parameterinfo[key][1]
            ylabel = self.parameterinfo[key][0]
            ystd[ystd<self.parameterinfo[key][1]] = self.parameterinfo[key][1]
        
        if problem == False:
            self.y       = y
            self.ystd    = ystd
            self.ylabel  = ylabel

    def Bin(self,nbins,lowerlimit=None,upperlimit=None,removeemptybins=True):
        if (hasattr(self,'x') == False) or (hasattr(self,'y')) == False:
            print 'Set X and Y first'
            return
        else:
            if np.ndim(self.x) > 1:
                x = self.x[:,0]
                y = self.y[:,0]
            else:
                x = self.x
                y = self.y
            ll = lowerlimit
            ul = upperlimit #handy shortcuts
            x,xstd,y,ystd,x_bins,binc = General.Bin(x,y,nbins,ll,ul)

        x    = np.array(x)
        xstd = np.array(xstd)
        y    = np.array(y)
        ystd = np.array(ystd)

        if removeemptybins:
            #remove any bins with no entries for 2d errorbar plot
            x0    = x != 0
            xstd0 = xstd != 0
            y0    = y != 0
            ystd0 = ystd != 0
            r = x0 & xstd0 & y0 & ystd0
            x    = x[r]
            xstd = xstd[r]
            y    = y[r]
            ystd = ystd[r]
       
        self.x = x
        self.xstd = xstd
        self.y = y
        self.ystd = ystd
        self.xbincentres = binc

    def CalculateFocusLocation(self):
        qm14ffindex = Parameters.bpminfo['QM14FF']
        ypos_before = self.r['cbpmy'][:,:,qm14ffindex]
        ypos_after  = self.r['cbpmy'][:,:,qm14ffindex+1]
        self.focuslocation,self.ebeamfocus = Orbit.CalculateEFocus(ypos_before,ypos_after)
        
    def JitterSubtract(self,focuslocation):
        #pull cbpm data
        qm14ffindex = Parameters.bpminfo['QM14FF']
        ypos_before = self.r['cbpmy'][:,:,qm14ffindex]
        ypos_after  = self.r['cbpmy'][:,:,qm14ffindex+1]
        xpos_before = self.r['cbpmx'][:,:,qm14ffindex]
        xpos_after  = self.r['cbpmx'][:,:,qm14ffindex+1]

        #calculate corrections
        self.xcorrections = Orbit.CalculateJitterCorrections(xpos_before,xpos_after,focuslocation)
        self.ycorrections = Orbit.CalculateJitterCorrections(ypos_before,ypos_after,focuslocation)
        #apply corrections and publish
        self.r['chaverjc'] = self.r['chaver'] - self.ycorrections
        self.parameterinfo['chaverjc'] = self.parameterinfo['chaver']

        self.r['chahorjc'] = self.r['chahor'] + self.xcorrections
        self.parameterinfo['chahorjc'] = self.parameterinfo['chahor']

        self.FilterApply()

        

