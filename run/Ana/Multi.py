import numpy as _np
import Data, Fit, General, Orbit, Parameters, Plot, Routines, Temp, Laser
from Routines import *
from Analysis import Analysis

class MultAnalysis:
    """
    Docstring

    """
    def __init__(self,filepathlist):
        self.analysislist = []
        for filepath in range(len(filepathlist)):
            a = Analysis(filepath)
            self.analysislist.append(a)

    def FilterAboveLimit(self,key,upperlimit=None):
        for instance in self.analysislist:
            instance.FilterAboveLimit(key,upperlimit)
    
    def FilterBelowLimit(self,key,lowerlimit=None):
        for instance in self.analysislist:
            instance.FilterBelowLimit(key,lowerlimit)
        
