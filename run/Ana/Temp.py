class Temp:
    def __init__(self):
        a = lwData.MultiData()
        filelist = os.listdir('../dat/tmp')
        for i in range(len(filelist)):
            filelist[i] = '../dat/tmp/'+filelist[i]
        a.TempMultiRead(filelist)
        self.data = a.totaldata
            
        t = self.data['time']
        self.t = t
        t2 = []
        for i in range(len(t)):
            t2.append(t[i].replace('_',''))
        
        self.x = [dateutil.parser.parse(s) for s in t2]
        self.pfn   = self.data['temppfn']
        self.ahall = self.data['tempahall']
        self.laser = self.data['templaser']
        self.lab   = self.data['templab']
        self.rack  = self.data['temprack']

        self.title = 'Temperature Log'
        self.suff  = ''
    
    def PlotAll(self):
                
        t = int(time.strftime('%Y%m%d%H%M'))
        startind   = 'beginning'
        stopind    = 'end'
        self.title = 'Temperature Log - All'
        self.suff  = 'all'
        self.PlotRange(startind,stopind)

    def PlotLastMonth(self):
        t = int(time.strftime('%m'))
        if t == 1:
            t2 = 12
        else:
            t2 = t-1
        t3 = str(t2)
        if len(t3) == 1:
            t3 = '0'+t3
        t = dateutil.parser.parse(time.strftime('%Y')+t3+time.strftime('%d%H%M'))
        ta = np.array(self.x)
        if self.x[0]>t:
            startind = 0
        else:
            startind = matplotlib.mlab.find(ta==t)

        stopind = 'end'
        self.title = 'Temperature Log - Last Month'
        self.suff  = 'month'
        self.PlotRange(startind,stopind)
    
    def PlotLastWeek(self):
        t = int(time.strftime('%d'))
        if t <= 7:
            t2 = 28
        else:
            t2 = t-7
        t3 = str(t2)
        if len(t3) == 1:
            t3 = '0'+t3
        t = dateutil.parser.parse(time.strftime('%Y%m')+t3+time.strftime('%H%M'))
        ta = np.array(self.x)
        if self.x[0]>t:
            startind = 0
        else:
            startind = matplotlib.mlab.find(ta==t)
        
        stopind = 'end'
        self.title = 'Temperature Log - Last Week'
        self.suff  = 'week'
        self.PlotRange(startind,stopind)

    def PlotLastDay(self):
        t = int(time.strftime('%d'))
        if t == 1:
            t2 = 28
        else:
            t2 = t-1
        t3 = str(t2)
        if len(t3) == 1:
            t3 = '0'+t3
        t = dateutil.parser.parse(time.strftime('%Y%m')+t3+time.strftime('%H%M'))
        ta = np.array(self.x)
        if self.x[0]>t:
            startind = 0
        else:
            startind = matplotlib.mlab.find(ta==t)
        
        stopind = 'end'
        self.title = 'Temperature Log - Last Day'
        self.suff  = 'day'
        self.PlotRange(startind,stopind)

    def PlotRange(self,start,stop):
        """Plot temperature over a specified range of dates

        Usage:

        PlotRange(201204010506,201204080134)

        Dates are YYYYDDMMHHMM
        or 'beginning' and 'end' work for start and stop
        respectively
        
        """

        if start == 'beginning':
            pass
        else:
            start      = int(start)
            self.x     = self.x[start:]
            self.pfn   = self.pfn[start:]
            self.ahall = self.ahall[start:]
            self.laser = self.laser[start:]
            self.lab   = self.lab[start:]
            self.rack  = self.rack[start:]

        if stop == 'end':
            pass
        else:
            stop       = int(stop)
            self.x     = self.x[:stop]
            self.pfn   = self.pfn[:stop]
            self.ahall = self.ahall[:stop]
            self.laser = self.laser[:stop]
            self.lab   = self.lab[:stop]
            self.rack  = self.rack[:stop]

        g = plt.figure(123)
        self.g = g
        plt.plot(self.x,self.pfn,'r-',label='PFN')
        plt.plot(self.x,self.ahall,'c-',label='A Hall')
        plt.plot(self.x,self.laser,'y-',label='Laser')
        plt.plot(self.x,self.lab,'b-',label='Lab')
        plt.plot(self.x,self.rack,'g-',label='Electronics')
        plt.title(self.title)
        plt.xlabel('Time')
        plt.ylabel('Temperature $^{\circ}$C')
        #plt.legend()
        g.set_figheight(6)
        g.set_figwidth(18)
        g.autofmt_xdate(rotation=-30,ha='left')
        g.show()

        t = time.strftime('%Y%m%d_%H%M')
        plotfn = '../dat/ana/tmp/'+t+'_temperature_'+self.suff+'.pdf'
        pl.savefig(plotfn,dpi=300,format=None)
        print 'Plot saved to ',plotfn
