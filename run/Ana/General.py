import numpy as np
from numpy import ma
import os
import string

import Data
    
def CheckFileExists(filename) :
    i = 1
    basefilename = filename[:-4]
    extension = filename[-4:]
    while os.path.exists(filename) :
        filename = basefilename+str(i)+extension
        i = i + 1
    return filename

def ListFilesContaining(directory,namestring):
    """
    ListFileContainting(directory,namestring)

    List the files in 'directory' whose name contains namestring

    returns list of files
    """
    input_flist  = os.listdir(directory)
    output_flist = []
    for f in input_flist:
        test = string.rfind(f,namestring)
        if test != -1:
            output_flist.append(f)
    return output_flist

def ListFiles(directory,extension):
    """
    ListFiles(directory,extension)

    List the files in 'directory' with the 3 letter extension
    described by 'extension'

    returns list of files
    """
    input_flist  = os.listdir(directory)
    output_flist = []
    for f in input_flist:
        if f[-3:] == extension:
            output_flist.append(f)
    return output_flist
        
def Correlation(x,y):
    xm = np.mean(x)
    ym = np.mean(y)
    xs = np.std(x)
    ys = np.std(y)
    L  = len(x)    
    correlation = np.sum((x-xm)*(y-ym))/((L-1)*xs*ys)
    return correlation

def Range(array) :
    min = np.min(array)
    max = np.max(array)
    ran = max-min
    return ran

def ReplaceValue(array,old,new):
    array[array == old] = new
    return array

def ReplaceZero(array,replacementvalue):
    r = replacementvalue
    array[array == 0] = r
    return array

def StdOfMean(array,axis=1):
    return np.array(np.std(array,axis=axis)/np.sqrt(np.shape(array)[1]))

def StdOfMean_any_axis(array,axis=0):
    """
    this function does any axis, but is much much slower!!!
    """
    n   = np.shape(array)[axis]
    f   = (1./(n*(n-1)))
    d   = ma.apply_along_axis(_Diff,axis,array)
    std = np.array(ma.sqrt(f*ma.sum(d*d,axis=axis)))
    return std
    
def Std(array,axis=0):
    n   = np.shape(array)[axis]
    f   = (1./(n-1))
    d   = ma.apply_along_axis(_Diff,axis,array)
    std = np.array(ma.sqrt(f*ma.sum(d*d,axis=axis)))
    return std

def Mean(array,axis=0):
    return np.array(ma.mean(array,axis=axis))

def _Diff(array):
    return array-np.mean(array)

def GetDirs():
    localsettings = Data.GetLocalSettings()
    return localsettings


def Bin(x,y,nbins,lowerlim=None,upperlim=None):
    """
    Bins(xarray,yarra,nbins)
    
    returns rx,rxstd,ry,rystd,x_bins
    rx     - xmean
    rxstd  - xstd
    ry     - ymean
    rystd  - ystd
    x_bins - cut off points in x => lenof = len(xarray) + 1
    """
    if lowerlim == None:
        lowerlim = np.min(x)
    if upperlim == None:
        upperlim = np.max(x)
    xstep      = (upperlim - lowerlim) /(nbins + 1.)
    x_bins     = np.arange(lowerlim, upperlim+1e-9, xstep)
    #calculate the bin centres by averaging all up to the last
    #bin with the array shifted back on in effect - or all but
    #the first bin
    bincentres = np.average([x_bins[1:],x_bins[:-1]],axis=0)
    #the bins are made by the min and max of the data - make
    #sure to include these points by ever so slightly expanding
    #the edge bins
    x_bins[0]  = x_bins[0]-1e-9
    x_bins[-1] = x_bins[-1]+1e-9
    rx,rxstd,ry,rystd = [],[],[],[]
    
    i = 0
    j = i+1
    while j < len(x_bins):
        subset1 = x >= x_bins[i]
        subset2 = x <  x_bins[j]
        subset  = subset1 & subset2
        xsub   = x[subset]
        ysub   = y[subset]
        xm   = xsub.mean()

        if np.isnan(xm):
            xm   = 0
        xstd = xsub.std()
        if np.isnan(xstd):
            xstd = 0
        ym   = ysub.mean()
        if np.isnan(ym):
            ym   = 0
        ystd = ysub.std()
        if np.isnan(ystd):
            ystd = 0
        rx.append(xm)
        rxstd.append(xstd)
        ry.append(ym)
        rystd.append(ystd)
        i += 1
        j += 1

    rxstd = np.array(rxstd)
    rxstd = np.array(rxstd/np.sqrt(len(rxstd)))
    rystd = np.array(rystd)
    rystd = np.array(rystd/np.sqrt(len(rystd)))
    
    return rx,rxstd,ry,rystd,x_bins,bincentres
