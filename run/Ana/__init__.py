
""" 
Analysis:
How to use    
a = Ana.Analysis('path/to/datafile.dat')
a.FilterBelowLimit('cherenkov',300)
a.FilterBelowLimit('refc1amp',500)
a.X('chaver','a')
a.Y('cherenkovcn','a')
a.PlotXYErrorBar()
a.FitOI(235,0)
a.PlotXYFitData()

DualAnalysis
How to use
a = Ana.DualAnalaysis('path/to/horizontalfile','path/to/verticalfile')
a.FilterBelowLimit('cherenkovcn',100)
a.FilterBelowLimit('refc1amp',100)
a.XDefault()
a.Y('cherenkovcn','a')
a.FitOI2D()

"""

# RHUL Cherenkov background detector analysis software
# based on ATF2 Extraction Line Laserwire Analysis Software V5
# V1
# 2015/06/18
# Author: Laurie Nevay

import numpy as np
import AnaData, General, Orbit, Parameters, Plot, Routines, Temp, Analysis
#import Fit #no minuit installed!
from Routines import *
from Analysis import Analysis


__all__ = ['Data','Fit','General','Orbit','Parameters','Plot','Routines']

def DefaultLaserAnalysis(datadirectory):
    Laser.DefaultAnalysis(datadirectory)




