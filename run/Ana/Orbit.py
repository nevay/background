from matplotlib import pyplot as plt
import numpy as _np
import Parameters


def CalculateFocus(predata,postdata,separation=1.5):
    m    = (postdata - predata)/separation
    x    = _np.arange(0,separation+1e-9,separation/1000)
    y    = _np.array([m[i]*x + predata[i] for i in range(len(m))]).transpose()
    ystd = _np.std(y,axis=1)
    
    focuslocation = _np.average(x,weights=(1/ystd**3))
    
    return x,y,ystd,focuslocation

def RecalculateIpAngle():
    Parameters.parameters['qm14qm13sep']
    
def CalculateEFocus(predata,postdata):
    separation    = Parameters.parameters['qm14qm13sep'][0]
    
    s             = _np.shape(postdata)
    postdata      = postdata.flatten()
    predata       = predata.flatten()

    postdata      = postdata - _np.mean(postdata)
    predata       = predata  - _np.mean(predata)
    
    d             = postdata - predata
    m             = d/separation
    x             = _np.arange(0,separation+1e-9,separation/1000)
    
    postdatastd   = Parameters.parameterinfo['cbpmy'][1]
    predatastd    = postdatastd
    # generic bpm resolution just now
    # in future cbpm resolution will be recorded in data and unique to each bpm and dimension
    separationstd = 0.001
    
    dstd          = _np.sqrt( ((postdata*postdatastd)**2) + ((predata*predatastd)**2) )
    mstd          = m * _np.sqrt( ((dstd/d)**2) + ((separationstd/separation)**2) )
    
    y             = _np.array([m[i]*x + predata[i] for i in range(len(m))]).transpose()
    ydisp         = _np.std(y,axis=1) # called dispersion, but actually standard deviation across vertical
    
    focuslocation = _np.average(x,weights=(1/ydisp**3))
    # high power makes it more accurate at finding the focus
    
    ebeamfocus = {}
    
    ebeamfocus['x']              = x
    ebeamfocus['y']              = y
    ebeamfocus['ydisp']          = ydisp
    ebeamfocus['m']              = [m,mstd]
    ebeamfocus['focuslocation']  = [focuslocation,100] #in microns, second value uncertainty 

    return focuslocation,ebeamfocus

def CalculateJitterCorrections(predata,postdata,focuslocation):
    separation    = Parameters.parameters['qm14qm13sep'][0]
    s           = _np.shape(predata)
    postdata    = postdata.flatten()
    predata     = predata.flatten()
    d           = postdata - predata
    m           = d/separation
    
    dx          = _np.array([m[i]*focuslocation + predata[i] for i in range(len(m))])
    #dx          = dx - _np.mean(dx)  #so can be used in multiscan
    dx          = dx.reshape(s)

    return dx
    
