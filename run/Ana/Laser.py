import numpy as np
import lwData
import minuit
import os
import matplotlib
import pickle

from matplotlib import pyplot as plt
from os.path import exists

import General
import Plot

class LaserAnalysis:
    """
    LaserAnalysis(labviewanalysisfilepath,m2detailsfilepath)
    

    """
    def __init__(self,datadirectory):
        # do some checks first
        if exists(datadirectory) != True:
            print 'Invalid data path - please correct'
            return
        files    = os.listdir(datadirectory)
        llvfiles = General.ListFilesContaining(datadirectory,'llv')
        if len(llvfiles) == 0:
            print 'No llv file type in the data directory'
            return
        m2detailsfiles = General.ListFilesContaining(datadirectory,'m2_details')
        if len(m2detailsfiles) == 0:
            print 'No m2details file in the data directory'
            return

        # load the data
        llvfilepath      = datadirectory+'/'+llvfiles[0]
        m2detailspath    = datadirectory+'/'+m2detailsfiles[0]
        d = lwData.Data()
        d.Read(llvfilepath)
        self.data        = d.data
        self.filename    = d.filename
        self.filepath    = d.filepath
        self.anadir      = datadirectory+'/'

        # load some other parameters and prepare them
        a = lwData.General()
        a.Read(m2detailspath)
        self.const       = a.data
        self.const['analysisangle_rad'] = (self.const['analysisangle']/180.0)*np.pi
        self.const['reversed'] = self.const['reversed'] == 1

        # prepare aboslute positions
        self.data['positionraw'] = self.data['position'] #keep raw copy
        if self.const['reversed'] == True:
            self.data['position'] = self.const['stagezerodistance'] - self.data['positionraw']
            print 'Reversed positions - negative = further from lens'
        else:
            self.data['position'] = self.const['stagezerodistance'] + self.data['positionraw']
            print 'Normal positions - positive = closer to lens'
        # make sure the data is in order for nicest plots
        for key in self.data:
            if key != 'position':
                self.data[key] = self.data[key][self.data['position'].argsort()]
        x = self.data['position'].sort()
        self.xdata = self.data['position']

        # set up some other variables
        self.data_scaled = {}
        self.error       = False
        
    def YData(self,key):
        """
        Maj = vertical / 'v' in Dataray
        Min = horizontal / 'u' in Dataray
        """
        
        if (key == 'clip1') | (key == 'clip2'):
            self.majdata    = self.data[key+'v']
            self.majdatastd = self.data[key+'vstd']
            self.mindata    = self.data[key+'u']
            self.mindatastd = self.data[key+'ustd']
        else:
            self.ydata = self.data[key]

    def FitParamsInit(self,model):
        if model == 'M2' : 
            maj_m2          = 1.20
            maj_xo          = np.average(self.xdata,weights=self.majdata)
            maj_wo          = np.min(self.majdata)
            min_m2          = 1.20
            min_xo          = np.average(self.xdata,weights=self.mindata)
            min_wo          = np.min(self.mindata)

            self.fpi        = {
                'maj_m2':maj_m2,
                'maj_xo':maj_xo,
                'maj_wo':maj_wo,
                'min_m2':min_m2,
                'min_xo':min_xo,
                'min_wo':min_wo
                }

    def FourSigma(self,m2,xo,wo,xdata,wavelength):
        y = 2*wo*np.sqrt(1.0+(((xdata-xo)*m2*wavelength)/(np.pi*(wo**2)))**2) 
        return y
        
    def FitM2(self,model='M2'):
        """
        Fit(model)
        model = 'M2'

        """
        wave = self.const['wavelength']
        self.FitParamsInit(model)
        
        imajm2 = self.fpi['maj_m2']
        imajxo = self.fpi['maj_xo']
        imajwo = self.fpi['maj_wo']
        iminm2 = self.fpi['min_m2']
        iminxo = self.fpi['min_xo']
        iminwo = self.fpi['min_wo']
        print self.fpi
        
        maj_fnc  = lambda majm2,majxo,majwo :2*majwo*np.sqrt(1.0+(((self.xdata-majxo)*majm2*wave)/(np.pi*(majwo**2)))**2) 
        maj_chi2 = lambda majm2,majxo,majwo : ((maj_fnc(majm2,majxo,majwo)-self.majdata)**2/self.majdatastd**2).sum()
        m_maj    = minuit.Minuit(maj_chi2,majm2=imajm2,majxo=imajxo,majwo=imajwo,limit_majm2=(1.0,10.0))
        min_fnc  = lambda minm2,minxo,minwo :2*minwo*np.sqrt(1.0+(((self.xdata-minxo)*minm2*wave)/(np.pi*(minwo**2)))**2) 
        min_chi2 = lambda minm2,minxo,minwo : ((min_fnc(minm2,minxo,minwo)-self.mindata)**2/self.mindatastd**2).sum()
        m_min    = minuit.Minuit(min_chi2,minm2=iminm2,minxo=iminxo,minwo=iminwo,limit_minm2=(1.0,10.0))
        
        m_maj.strategy     = 1
        m_maj.tol          = 10
        m_maj.printMode    = 1
        m_min.strategy     = 1
        m_min.tol          = 10
        m_min.printMode    = 1
        
        print 'Fitting Major Axis\n'

        try:
            m_maj.migrad()
            m_maj.hesse()
            self.m_maj = m_maj
        except:
            print 'Uh oh - fitting error'
            self.error = True
            return
        print 'Fitting Major Axis\n'
        try:
            m_min.migrad()
            m_min.hesse()
            self.m_min = m_min
        except:
            print 'Uh oh - fitting error'
            self.error = True
            return
 
        if self.error == False:
            self.fitparams       = {} # each value has array([parameter,uncertainty])
            self.fitparams_nonsi = {}
            for key in m_maj.values:
                self.fitparams[key] = [m_maj.values[key],m_maj.errors[key]]
                if key == 'majm2':
                    self.fitparams_nonsi[key] = [m_maj.values[key],m_maj.errors[key]]
                elif key == 'majwo':
                    self.fitparams_nonsi[key] = [m_maj.values[key]*1000000.,m_maj.errors[key]*1000000.]
                elif key == 'majxo':
                    self.fitparams_nonsi[key] = [m_maj.values[key]*1000.,m_maj.errors[key]*1000.]
            for key in m_min.values:
                self.fitparams[key] = [m_min.values[key],m_min.errors[key]]
                if key == 'minm2':
                    self.fitparams_nonsi[key] = [m_min.values[key],m_min.errors[key]]
                elif key == 'minwo':
                    self.fitparams_nonsi[key] = [m_min.values[key]*1000000.,m_min.errors[key]*1000000.]
                elif key == 'minxo':
                    self.fitparams_nonsi[key] = [m_min.values[key]*1000.,m_min.errors[key]*1000.]
            fp = self.fitparams           

            self.maj_model  = maj_fnc(fp['majm2'][0],fp['majxo'][0],fp['majwo'][0])
            self.min_model  = min_fnc(fp['minm2'][0],fp['minxo'][0],fp['minwo'][0])
            self.projectedfoursigma = self.ProjectedSigma(self.min_model,self.maj_model,self.const['analysisangle_rad'])
            self.projectedsigma     = self.projectedfoursigma/4.0

    def ProjectedSigma(self,sigma_z,sigma_y,theta):
        v = sigma_y
        h = sigma_z
        psig = np.sqrt(((np.sin(theta))*h)**2 + ((np.cos(theta))*v)**2)
        return psig

    def ScaleData(self):
        fo                                 = self.const['m2lensfocallength']
        fs                                 = self.const['lwlensfocallength']
        ratio                              = fs/fo
        fp                                 = self.fitparams

        self.fitparams_scaled              = {}
        self.fitparams_scaled['majxo']     = 1. / ((1/fs) - (1/fo) - (1/fp['majxo'][0]))
        self.fitparams_scaled['minxo']     = 1. / ((1/fs) - (1/fo) - (1/fp['minxo'][0]))
        self.fitparams_scaled['majwo']     = fp['majwo'][0]*ratio
        self.fitparams_scaled['minwo']     = fp['minwo'][0]*ratio
        self.fitparams_scaled['majm2']     = fp['majm2'][0]
        self.fitparams_scaled['minm2']     = fp['minm2'][0]

        fps                                = self.fitparams_scaled
        self.data_scaled['position']       = 1 / ((1/fs) - (1/fo) - (1/self.data['position']))
        self.data_scaled['maj_sigma']      = self.majdata*ratio/4.0
        self.data_scaled['min_sigma']      = self.mindata*ratio/4.0
        self.data_scaled['maj_sigmastd']   = self.majdatastd*ratio/4.0
        self.data_scaled['min_sigmastd']   = self.mindatastd*ratio/4.0
        self.data_scaled['maj_sigmamodel'] = self.FourSigma(fps['majm2'],fps['majxo'],fps['majwo'],self.data_scaled['position'],self.const['wavelength'])/4.0
        self.data_scaled['min_sigmamodel'] = self.FourSigma(fps['minm2'],fps['minxo'],fps['minwo'],self.data_scaled['position'],self.const['wavelength'])/4.0
        self.data_scaled['projectedsigma'] = self.ProjectedSigma(self.data_scaled['min_sigmamodel'],self.data_scaled['maj_sigmamodel'],self.const['analysisangle_rad'])

    def WriteNormalOutput(self):
        fd_filename = self.anadir+'output.dat'
        a = lwData.General()
        dd = dict(self.fitparams.items())
        dd['majmodel']   = self.maj_model
        dd['minmodel']   = self.min_model
        dd['majdata']    = self.majdata
        dd['mindata']    = self.mindata
        dd['majdatastd'] = self.majdatastd
        dd['mindatastd'] = self.mindatastd
        dd['position']   = self.xdata
        a.Write(fd_filename,dd)

    def WriteScaledOutput(self):
        fd_filename = self.anadir+'output_scaled.dat'
        a = lwData.General()
        dd = {}
        for key in self.fitparams_scaled:
            newkey = 'scaled_'+str(key)
            dd[newkey] = self.fitparams_scaled[key]
        for key in self.data_scaled:
            newkey = 'scaled_'+str(key)
            dd[newkey]   = self.data_scaled[key]
        a.Write(fd_filename,dd)

    def WriteScaledParamsForlwAna(self):
        """
        Here assming major is y (vertical) dimension and minor is z (horizontal)
        Writes units in micron (z0, sigma0,wavelength)
        Writes angle in degrees
        """


        #need scaled fit xo as difference from centre
        #ie 0 is the middle of the model - not the distance
        #from the lens

        #cen didn't work well!  so just use nearest xdata point to min(projectedsigma)
        #cen = np.average(self.data_scaled['position'],weights=self.data_scaled['projectedsigma']**2)
        cen2 = self.data_scaled['position'][np.argmin(self.data_scaled['projectedsigma'])]
        
        fp = self.fitparams_scaled
        ps = {}
        ps['m2z']        = fp['minm2']
        ps['m2y']        = fp['majm2']
        ps['z_0z']       = (fp['minxo'] - cen2)/1.0e-6
        ps['z_0y']       = (fp['majxo'] - cen2)/1.0e-6
        ps['sigma_0_z']  = fp['minwo']/2.0e-6 #convert w to sigma
        ps['sigma_0_y']  = fp['majwo']/2.0e-6 #convert w to sigma
        ps['theta']      = self.const['analysisangle']
        ps['m2general']  = ps['m2y']
        ps['wavelength'] = self.const['wavelength']/1e-6
        ps['sex']        = 235.0
        ps['dx']         = 0.0

        orderlist = ['m2z','m2y','z_0z','z_0y','sigma_0_z','sigma_0_y','theta','m2general','wavelength','sex','dx']

        a = lwData.General()
        fname = self.anadir+'fit_parameters_input.dat'
        a.WriteInOrder(fname,ps,orderlist)

        #write covariance matrix as a pickled dictionary
        cov = {}
        cov['maj'] = self.m_maj.covariance
        cov['min'] = self.m_min.covariance
        f = open(self.anadir+'covariance.dat','w')
        pickle.dump(cov,f)
        f.close()
        
        
    def PlotM2FitData(self) :
 
        plttitle      = self.filename.replace('_',' ')
        pltarea       = Plot.PlotArea(self.xdata,self.mindata)
        paramsloc     = Plot.PlotFitLegLoc(pltarea)
        fittext       = Plot.FitText(self.fitparams_nonsi)
        
        plt.figure()
        plt.errorbar(self.xdata,self.mindata,yerr=self.mindatastd,fmt='o',linewidth=1,mew=1,mec='g',ms=3,color='g',label='Minor',alpha=0.4)
        plt.errorbar(self.xdata,self.majdata,yerr=self.majdatastd,fmt='o',linewidth=1,mew=1,mec='b',ms=3,color='b',label='Major',alpha=0.4)
        plt.plot(self.xdata,self.min_model,color='g',label='Model',lw=1)
        plt.plot(self.xdata,self.maj_model,color='b',label='Model',lw=1)
        
        plt.title(plttitle)
        plt.xlabel('Position (m)')
        plt.ylabel('4$\sigma$ (m)')
        plt.legend()

        xloc = np.mean(self.xdata)
        yloc = np.max(self.majdata)*0.8
        plt.text(xloc,yloc,fittext,bbox=dict(facecolor='white'),size='small')
        
        plotfn = self.anadir+self.filename+'_m2_fit_plot.pdf'
        plotfn = General.CheckFileExists(plotfn)
        twikfn = plotfn[:-4]+'.png'
        plt.savefig(plotfn,dpi=300,format=None)
        plt.savefig(twikfn)
        plt.show()
        print 'Plot saved to '+plotfn
        print 'Plot saved to '+twikfn

    def PlotScaledData(self):
        plttitle      = self.filename.replace('_',' ')
                
        pos    = self.data_scaled['position']/0.001
        maj    = self.data_scaled['maj_sigma']/0.000001
        min    = self.data_scaled['min_sigma']/0.000001
        majerr = self.data_scaled['maj_sigmastd']/0.000001
        minerr = self.data_scaled['min_sigmastd']/0.000001
        
        plt.figure()
        plt.errorbar(pos,min,yerr=minerr,fmt='o',linewidth=1,mew=1,mec='g',ms=3,color='g',label='Scaled Minor',alpha=0.4)
        plt.errorbar(pos,maj,yerr=majerr,fmt='o',linewidth=1,mew=1,mec='b',ms=3,color='b',label='Scaled Major',alpha=0.4)
                
        plt.title(plttitle)
        plt.xlabel('Position (mm)')
        plt.ylabel('$\sigma$ ($\mu$m)')
        plt.legend()

        plotfn = self.anadir+self.filename+'_scaled_data.pdf'
        plotfn = General.CheckFileExists(plotfn)
        twikfn = plotfn[:-4]+'.png'
        plt.savefig(plotfn,dpi=300,format=None)
        plt.savefig(twikfn)
        plt.show()
        print 'Plot saved to '+plotfn
        print 'Plot saved to '+twikfn

    def PlotScaledModel(self):
        plttitle      = self.filename.replace('_',' ')
                
        pos    = self.data_scaled['position']/0.001
        maj    = self.data_scaled['maj_sigmamodel']/0.000001
        min    = self.data_scaled['min_sigmamodel']/0.000001
        pro    = self.data_scaled['projectedsigma']/0.000001
        
        plt.figure()
        plt.plot(pos,maj,color='g',label='Major',lw=1)
        plt.plot(pos,min,color='b',label='Minor',lw=1)
        plt.plot(pos,pro,color='r',label='Projected',lw=1)
        
        plt.title(plttitle)
        plt.xlabel('Position (mm)')
        plt.ylabel('$\sigma$ ($\mu$m)')
        plt.legend()

        plotfn = self.anadir+self.filename+'_projected.pdf'
        plotfn = General.CheckFileExists(plotfn)
        twikfn = plotfn[:-4]+'.png'
        plt.savefig(plotfn,dpi=300,format=None)
        plt.savefig(twikfn)
        plt.show()
        print 'Plot saved to '+plotfn
        print 'Plot saved to '+twikfn
    
def ProjectedSigma(sigma_z,sigma_y,theta):
    v = sigma_y
    h = sigma_z
    psig = np.sqrt(((np.sin(angle))*h)**2 + ((np.cos(angle))*v)**2)
    return psig

def DefaultAnalysis(datadirectory):
    a = LaserAnalysis(datadirectory)
    a.YData('clip2')
    a.FitM2('M2')
    a.ScaleData()
    a.PlotM2FitData() 
    a.PlotScaledData()
    a.PlotScaledModel()
    a.WriteNormalOutput()
    a.WriteScaledOutput()
    a.WriteScaledParamsForlwAna()

def GenerateCovarianceMatrix(dualcovdict):
    """
    returns 2 2D numpy arrays - major and minor axes covariance matrices
    """
    maj = dualcovdict['maj']
    min = dualcovdict['min']

    #dict is accessed with tuple pair of strings
    majcovl1 = [maj[('majm2','majm2')],maj[('majm2','majwo')],maj[('majm2','majxo')]]
    majcovl2 = [maj[('majwo','majm2')],maj[('majwo','majwo')],maj[('majwo','majxo')]]
    majcovl3 = [maj[('majxo','majm2')],maj[('majxo','majwo')],maj[('majxo','majxo')]]
    majcov   = np.array([majcovl1,majcovl2,majcovl3])

    mincovl1 = [min[('minm2','minm2')],min[('minm2','minwo')],min[('minm2','minxo')]]
    mincovl2 = [min[('minwo','minm2')],min[('minwo','minwo')],min[('minwo','minxo')]]
    mincovl3 = [min[('minxo','minm2')],min[('minxo','minwo')],min[('minxo','minxo')]]
    mincov   = np.array([mincovl1,mincovl2,mincovl3])

    return majcov, mincov
