
#import pylab as pl
#import matplotlib
#import matplotlib.pyplot as plt
#import time
#import os
#import minuit
#import dateutil

#import lwData
#import OrderedDict
#import lwIntegral
#import lwIntegral_parallel

#from OrderedDict import OrderedDict as od
#from matplotlib.ticker import MultipleLocator
#from matplotlib.font_manager import fontManager, FontProperties
#from matplotlib import *
#from numpy import array
#from numpy import ndarray

#flatten = np.ndarray.flatten




    ###   WRITING METHODS BELOW HERE
    def WriteXYData(self):
        datafilename  = self.anadir+self.filename+'_xy_data.csv'
        datafilename  = CheckFileExists(datafilename)
        f = open(datafilename,'a',0)
        
        titlestowrite = [self.xlabel,'std',self.ylabel,'std']
        datatowrite   = np.transpose(np.array([self.x,self.xstd,self.y,self.ystd]))
        
        f.write('\t'.join(titlestowrite)+'\n')
        for row in datatowrite:
            linetowrite = '\t'.join(map(str,row))+'\n'
            f.write(linetowrite)
        f.close()
        del datatowrite
        print 'XY data written to '+datafilename+'\n'

    def WriteXYFitData(self):
        datafilename  = self.anadir+self.filename+'_xyfit_data.csv'
        datafilename  = CheckFileExists(datafilename)
        f = open(datafilename,'a',0)
        f.write('FitParameters>\n')
        for key in self.fitparams:
            linestring = key+'>\t'+'\t'.join(map(str,self.fitparams[key]))+'\n'
            self.f.write(linestring)
        f.write('\n\n\n')
        
        titlestowrite = [self.xlabel,'std',self.ylabel,'std','yfitmodel']
        datatowrite   = np.transpose(np.array([self.x,self.xstd,self.y,self.ystd,self.ymodel]))
        
        f.write('\t'.join(titlestowrite)+'\n')
        for row in datatowrite:
            linetowrite = '\t'.join(map(str,row))+'\n'
            f.write(linetowrite)
        f.close()
        del datatowrite
        print 'XY data written to '+datafilename+'\n'

        #needs to updated!!!
    def WriteAvgData(self) :
        #needs to updated!!!
        datafilename = self.anadir+self.filename+'_avg_data.csv'
        datafilename = CheckFileExists(datafilename)
        f = open(datafilename,'a',0)
        if hasattr(self,'dataavg') == False:
            self.DataAvg(self.data)
            self.DataStd(self.data)
        else: 
            pass        
        datatowrite = np.array([[]])
        keystowrite = []
        for key, value in self.dataavg.iteritems():
            if np.ndim(value) == 1:
                np.append(datatowrite,value)
                keystowrite.append(key)
            elif np.ndim(value) > 1:
                pass
        
        f.write('\t'.join(keystowrite)+'\n')
        datatowrite = np.transpose(datatowrite)
        for row in datatowrite:
            linetowrite = '\t'.join(map(str,row))+'\n'
            f.write(linetowrite)
        f.close()
        del datatowrite
        print 'Average data written to '+datafilename+'\n'



def FitOIHorizontal(x,y,ystd,filename,sey=1.0,dy=0.0):
    """
    FitOIHorizontal(x,y,ystd,filename,sex=1.0,dy=0.0)

    Fits the overlap integral using the empirical laser propagation

    returns the fit paramaters as a dictionary and the ymodel array

    """
    i_off = np.min(y)
    i_cen = np.mean(x)
    i_amp = 2*(np.mean(y)-i_off)
    i_sex = 100

    fnc  = lambda offset,centre,amplitude,sex : lwIntegral2.OISetEH(x,0,sex,sey,amplitude,offset,centre)
    chi2 = lambda offset,centre,amplitude,sex : ((fnc(offset,centre,amplitude,sex)-y)**2/ystd**2).sum()
    m    = minuit.Minuit(chi2,offset=i_off,centre=i_cen,amplitude=i_amp,sex=i_sex)
    m.tol = 10.0
    m.up  = 1.0

    m.maxcalls     = 1000
    m.strategy     = 1
    m.printMode    = 1

    m.migrad()
    m.hesse()
        
    fitparams = {} # each value has array([parameter,uncertainty])
    for key in m.values:
        fitparams[key] = [m.values[key],m.errors[key]]
    ps              = np.array(fitparams.values())[:,0]
    fp              = fitparams
    ymodel = fnc(fp['offset'][0],fp['centre'][0],fp['amplitude'][0],fp['sex'][0])
    fchi2  = chi2(fp['offset'][0],fp['centre'][0],fp['amplitude'][0],fp['sex'][0])
    fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]

    return fitparams,ymodel,y


def FitOIM2Basic(x,y,ystd,m2=1.0,sex=235.0,dx=0.0):
    """
    FitOIM2(x,y,ystd,filename,m2,sex=235.0,dx=0.0)

    Fits the overlap integral using a single vertical M2 only

    returns the fit parameters as a dictionary and the ymodel array
    
    """
    a = lwData.Fit()
    a.WriteFitDataInput(x,y,ystd)
    a.UpdateFitParams(sex,dx)
    a.UpdateM2Param(m2)
    os.chdir('oi4')
    process = subprocess.Popen('./main -l',shell=True)
    process.wait()
    os.chdir('../')
    fitparams = a.ReadFitParametersOutput()
    y_model,signal,signal_err,signal_model = a.ReadFitDataOutput()
    return fitparams,signal_model,y_model
