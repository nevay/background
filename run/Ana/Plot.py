import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from matplotlib.ticker import MultipleLocator
from matplotlib.font_manager import fontManager, FontProperties
from matplotlib import *
from mpl_toolkits.mplot3d import Axes3D
from numpy import array
from numpy import ndarray

import General
import Parameters

flatten = numpy.ndarray.flatten

def ControlPlot(rawdata,datainfo,savedir,filename):
    raw = rawdata
    a   = plt.figure(1000,figsize=(18,10))
    suptitlestring       = filename.replace('_',' ')+' Control Plot'
    a.suptitle(suptitlestring,size='xx-large')
    a.subplotpars.wspace = 0.30
    a.subplotpars.hspace = 0.31
    if datainfo['type'] == 'log':
        infostring = 'Log of '+str(int(datainfo['noofstep']))+' Pulses'
    elif datainfo['type'] == 'lws':
        axisstring  = 'Axis: '       +datainfo['axis']
        sampstring  = 'Samples: '    +str(datainfo['noofsamp'])
        startstring = 'Start: '      +str(datainfo['start'])
        stopstring  = 'Stop: '       +str(datainfo['stop'])
        stepstring  = '# of Steps: '+str(datainfo['noofstep'])
        infostring = ', '.join([axisstring,sampstring,startstring,stopstring,stepstring])
     
    plt.figtext(0.5,0.05,infostring,horizontalalignment='center',size='x-large')
            
    # Raw cherenov
    plt.subplot(5,4,1)
    plt.plot(flatten(raw['cherenkov']))
    plt.xticks([])
    plt.yticks([0,5000,10000,15000])
    plt.ylim(0,17000)
    plt.title('Cherenkov',size='medium')
    
    # REF1 amp
    plt.subplot(5,4,2)
    plt.plot(flatten(raw['refc1amp']))
    plt.xticks([])
    plt.yticks([0,1000,2000,3000])
    plt.ylim(0,3000)
    plt.title('REFC1:amp',size='medium')
    
    # laser energy 
    plt.subplot(5,4,3)
    plt.plot(flatten(raw['laseren'])*1000)
    plt.xticks([])
    plt.title('Laser Energy (mJ)',size='medium')
    
    # wirescanner detector
    plt.subplot(5,4,4)
    plt.plot(flatten(raw['wsdetector']))
    plt.xticks([])
    plt.yticks([0,5000,10000,15000])
    plt.ylim(0,17000)
    plt.title('WS Detector',size='medium')
    
    # chamber horizontal
    plt.subplot(5,4,5)
    plt.plot(flatten(raw['chahor']))
    plt.xticks([])
    plt.title('Chamber Horirzontal ($\mu$m)',size='medium')
    
    # chamber vertical
    plt.subplot(5,4,6)
    plt.plot(flatten(raw['chaver']))
    plt.xticks([])
    plt.title('Chamber Vertical ($\mu$m)',size='medium')
    
    # manipulator vertical
    plt.subplot(5,4,7)
    plt.plot(raw['manver'])
    plt.xticks([])
    #plt.yticks([0,20000,40000])
    #plt.ylim(0,45000)
    plt.title('Manipulator Vertical ($\mu$m)',size='medium')
    
    # manipulator angle
    plt.subplot(5,4,8)
    plt.plot(flatten(raw['manang']))
    plt.xticks([])
    plt.yticks([-0.5,0.5])
    plt.ylim(-1,1)
    plt.title('Manipulator Angle ($^{o}$)',size='medium')
    
    # ip offset x
    plt.subplot(5,4,10)
    plt.plot(flatten(raw['ipoffsetx']))
    plt.xticks([])
    plt.title('Ip Offset Horizontal ($\mu$m)',size='medium')
    
    # ip offset y
    plt.subplot(5,4,9)
    plt.plot(flatten(raw['ipoffsety']))
    plt.xticks([])
    plt.title('Ip Offset Vertical ($\mu$m)',size='medium')
    
    # ip angle x
    plt.subplot(5,4,12)
    plt.plot(flatten(raw['ipanglex']))
    plt.xticks([])
    plt.title('Ip Angle Horizontal ($\mu$rad)',size='medium')
    
    # ip angle y
    plt.subplot(5,4,11)
    plt.plot(flatten(raw['ipangley']))
    plt.xticks([])
    plt.title('Ip Angle Vertical ($\mu$rad)',size='medium')
    
    # x orbit
    plt.subplot(5,2,7)
    s   = np.shape(raw['cbpmx'])
    cx  = np.reshape(raw['cbpmx'],(s[0]*s[1],s[2]))
    cxt = cx.transpose()
    plt.plot(cxt)
    plt.xticks([0,10,20,30,40])
    plt.yticks([-2000,-1000,0,1000,2000])
    plt.ylim(-2000,2000)
    plt.title('CBPM X',size='medium')
    
    # y orbit
    plt.subplot(5,2,9)
    s   = np.shape(raw['cbpmy'])
    cy  = np.reshape(raw['cbpmy'],(s[0]*s[1],s[2]))
    cyt = cy.transpose()
    plt.plot(cyt)
    plt.xticks([0,10,20,30,40])
    plt.yticks([-2000,-1000,0,1000,2000])
    plt.ylim(-2000,2000)
    plt.title('CBPM Y',size='medium')
    
    # x orbit local
    plt.subplot(5,2,8)
    cxl   = np.delete(cx[:,14:19],2,axis=1)
    cxlt  = cxl.transpose()
    plt.plot(cxlt)
    labels = ['QM15FF','QM14FF','QM13FF','QM12FF']
    plt.xticks([0,1,2,3],labels)
    plt.axvline(1.5)
    plt.title('CBPM X Local',size='medium')
    
    # y orbit local
    plt.subplot(5,2,10)
    cyl   = np.delete(cy[:,14:19],2,axis=1)
    cylt  = cyl.transpose()
    plt.plot(cylt)
    labels = ['QM15FF','QM14FF','QM13FF','QM12FF']
    plt.xticks([0,1,2,3],labels)
    plt.axvline(1.5)
    plt.title('CBPM Y Local',size='medium')
    
    plotfn = savedir+filename+'_control_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Control plot saved to ',plotfn
    print 'Control plot saved to ',twikfn
    plt.show(block=False)

def SingleBpmCorrPlot(plotobject,row,col,ind,xdata1,xdata2,ydata,bpmname):

    ax1 = plotobject.add_subplot(row,col,ind+1)
    ax2 = ax1.twiny()
    ax1.plot(xdata1,ydata,'r+',alpha=0.4)
    ax1.set_xticks([])
    ax2.plot(xdata2,ydata,'b+',alpha=0.4) #matches our edm
    ax2.set_xticks([])
    plt.title(bpmname)
    plt.yticks([])
    plt.show(block=False)
        

def PlotBpmCorrelation(atf2xpos,atf2ypos,cherenkov,savedir,filename):
    x = atf2xpos
    y = atf2ypos
    s = cherenkov
    
    nrow  = 6
    ncol  = 10
    
    fig = plt.figure(figsize=(18,18))
    fig.suptitle('Signal BPM Correlations',size='x-large')
    fig.subplotpars.wspace = 0.30
    fig.subplotpars.hspace = 0.31
    
    for i in range(len(atf2xpos)):
        bpmname = Parameters.BpmNameAtf2(i)
        SingleBpmCorrPlot(fig,nrow,ncol,i,x[:,i],y[:,i],s,bpmname)

    plt.figtext(0.5,0.055,'Horizontal',horizontalalignment='center',size='xx-large',color='r')
    plt.figtext(0.5,0.035,'Vertical',horizontalalignment='center',size='xx-large',color='b')
    plt.figtext(0.1,0.5,'Cherenkov Signal',verticalalignment='center',rotation=90,size='xx-large')
    
    plotfn = savedir+filename+'_bpm_correlation.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=600,format=None)
    plt.savefig(twikfn)
    plt.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotCorrelation(filtereddata,savedir,filename):
    filtered      = filteredata
    
    cherenkov     = filtered['cherenkov'].flatten().compressed()
    charge        = filtered['refc1amp'].flatten().compressed()
    laseren       = filtered['laseren'].flatten().compressed()
    laserpd       = filtered['laserpd'].flatten().compressed()
    
    plt.figure()
    suptitlestring = filename.replace('_',' ')+' Correlation Plot'
    plt.suptitle(suptitlestring,size='xx-large')
    plt.subplots_adjust(wspace=0.3,hspace=0.3)
    
    plt.subplot(221)
    plt.plot(charge,cherenkov,'.',alpha=0.6)
    plt.xlabel('REFC1:amp (ADC Counts)')
    plt.ylabel('Cherenkov (ADC Counts)')
    plt.subplot(222)
    plt.plot(laseren,cherenkov,'.',alpha=0.6)
    plt.xlabel('Laser Energy (mJ)')
    plt.ylabel('Cherenkov (ADC Counts)')
    plt.subplot(223)
    plt.plot(laserpd,cherenkov,'.',alpha=0.6)
    plt.xlabel('Photodide (ADC Counts)')
    plt.ylabel('Cherenkov (ADC Counts)')
    plt.subplot(224)
    plt.plot(laseren,cherenkov/charge,'.',alpha=0.6)
    plt.xlabel('Laser Energy (mJ)')
    plt.ylabel('Charged Norm. Cherenkov (A.U.')
    
    plotfn = savedir+filename+'_correlation_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    plt.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotCCL(filteredata,savedir,filename):
    filtered      = filteredata
    plttitle      = filename.replace('_',' ')
    
    cherenkov     = filtered['cherenkov'].flatten().compressed()
    charge        = filtered['refc1amp'].flatten().compressed()
    laseren       = filtered['laseren'].flatten().compressed()
    
    fig = plt.figure()
    fig.set_figheight(10)
    fig.set_figwidth(6)
    fig.suptitle(plttitle,size='x-large')
    
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)
    
    ax1.plot(cherenkov,color='r',label='Cherenkov')
    ax1.set_ylabel('ADC Counts')
    ax1.legend()
    ax1.set_xlim(0,np.max(len(cherenkov)))
    ax2.plot(charge,color='g',label='REFC1:amp')
    ax2.set_ylabel('ADC Counts')
    ax2.legend()
    ax2.set_xlim(0,np.max(len(cherenkov)))
    ax3.plot(laseren,color='b',label='Laser Pulse Energy')
    ax3.set_xlabel('Pulse Number')
    ax3.set_ylabel('Energy')
    ax3.legend()
    ax3.set_xlim(0,np.max(len(cherenkov)))
    fig.subplots_adjust(left=0.15)
    fig.subplots_adjust(right=0.95)
    fig.subplots_adjust(bottom=0.05)
    
    plotfn = savedir+filename+'_ccl_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    fig.savefig(plotfn,dpi=300,format=None)
    fig.savefig(twikfn)
    fig.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotBpmXYHist(x,y,savedir,filename,bpmname) :
    plttitle = filename+'   '+bpmname
    
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left+width+0.02
    
    rect_scatter = [left,bottom,width,height]
    rect_histx   = [left,bottom_h,width,0.2]
    rect_histy   = [left_h,bottom,0.2,height]
    
    plt.figure(figsize=(8,8))
    
    axScatter = plt.axes(rect_scatter)
    axHistX   = plt.axes(rect_histx)
    axHistY   = plt.axes(rect_histy)
    
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)
    
    axScatter.scatter(x,y)
    axHistx.hist(x,bins=20,normed=True,histtype='bar',rwidth=0.8,align='mid',edgecolor='none',facecolor='b')
    axHisty.hist(y,bins=20,normed=True,histtype='bar',rwidth=0.8,align='mid',edgecolor='none',facecolor='b',orientation='horizontal')
    
    plotfn = savedir+filename+'_'+bpmname+'_histogram.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotBpmLocalAverage(x,xstd,y,ystd,savedir,filename) :
    startindex    = 14
    stopindex     = 19
    fb2ffindex    = 2 #of subarray

    x          = np.delete(np.transpose(np.average(x[:,startindex:stopindex],axis=0)),fb2ffindex)
    y          = np.delete(np.transpose(np.average(y[:,startindex:stopindex],axis=0)),fb2ffindex)
    xstd       = np.delete(np.transpose(np.average(xstd[:,startindex:stopindex],axis=0)),fb2ffindex)
    ystd       = np.delete(np.transpose(np.average(ystd[:,startindex:stopindex],axis=0)),fb2ffindex)
    bpmnumber  = np.delete(range(stopindex-startindex),fb2ffindex)
    labels     = ['QM15FF','QM14FF','','QM13FF','QM12FF']
        
    majorLocator = MultipleLocator(1)
    minorLocator = MultipleLocator(0.5)
    plttitle     = filename.replace('_',' ')
        
    lwiplinex    = 2.1
    lwiplabelx   = lwiplinex + 0.1
    lwiplabely   = np.min(x-xstd)-0.1*(General.Range(y))
            
    tv1 = plt.figure()
    tv2 = plt.subplot(111)
    plt.errorbar(bpmnumber,x,yerr=xstd,label='X',fmt='r.',linestyle='-')
    plt.errorbar(bpmnumber,y,yerr=ystd,label='Y',fmt='b.',linestyle='-',mew=1)
    plt.axvline(x=lwiplinex,linewidth=2,color='g')
    plt.text(lwiplabelx,lwiplabely,'LWIP',rotation=90,color='g',size='x-large')
    plt.title(plttitle)
    plt.xlabel('BPM')
    plt.ylabel('BPM Position ($\mu$m)')
    plt.legend()
    tv2.xaxis.set_major_locator(majorLocator)
    tv2.xaxis.set_minor_locator(minorLocator)
    plt.grid('true',which='minor')
    plt.grid('true',which='major')
    plt.xticks(range(stopindex-startindex),labels,rotation=90)
    tv1.set_figheight(6)
    tv1.set_figwidth(18)
    tv1.subplots_adjust(bottom=0.25)
        
    plotfn = savedir+filename+'_bpm_plot_local.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    tv1.savefig(plotfn,dpi=300,format=None)
    tv1.savefig(twikfn)
    tv1.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotBpmLocalRaw(x,y,savedir,filename) :
    startindex    = 14
    stopindex     = 19
    fb2ffindex    = 2 #of subarray

    x          = np.delete(x[:,startindex:stopindex],fb2ffindex,axis=1)
    y          = np.delete(y[:,startindex:stopindex],fb2ffindex,axis=1)
    bpmnumber  = np.delete(range(stopindex-startindex),fb2ffindex)
    labels     = ['QM15FF','QM14FF','QM13FF','QM12FF']
        
    majorLocator = MultipleLocator(1)
    minorLocator = MultipleLocator(0.5)
    plttitle     = filename.replace('_',' ')
        
    lwiplinex    = 1.5
    
    tv1 = plt.figure()
    tv1.suptitle(plttitle,size='x-large')
    
    tv2 = plt.subplot(211)
    plt.axvline(x=lwiplinex,linewidth=2,color='g')
    plt.ylabel('Horizontal ($\mu$m)')
    plt.plot(x.transpose())
    tv2.xaxis.set_major_locator(majorLocator)
    tv2.xaxis.set_minor_locator(minorLocator)
    plt.grid('true',which='minor')
    plt.grid('true',which='major')
    plt.xticks([])
    
    tv3 = plt.subplot(212)
    plt.axvline(x=lwiplinex,linewidth=2,color='g')
    plt.plot(y.transpose())
    plt.ylabel('Vertical ($\mu$m)')
    tv3.xaxis.set_major_locator(majorLocator)
    tv3.xaxis.set_minor_locator(minorLocator)
    plt.grid('true',which='minor')
    plt.grid('true',which='major')
    plt.xticks(range(stopindex-startindex-1),labels,rotation=90)

    plt.xlabel('BPM')
    tv1.subplots_adjust(bottom=0.2)
            
    plotfn = savedir+filename+'_bpm_plot_local.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    tv1.savefig(plotfn,dpi=300,format=None)
    tv1.savefig(twikfn)
    tv1.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 

def PlotBpmGlobal(x,xstd,y,ystd,savedir,filename) :
    
    x      = np.transpose(np.average(x,axis=0))
    y      = np.transpose(np.average(y,axis=0))
    xstd   = np.transpose(np.average(xstd,axis=0))
    ystd   = np.transpose(np.average(ystd,axis=0))
        
    majorLocator = MultipleLocator(5)
    minorLocator = MultipleLocator(1)
    plttitle     = filename.replace('_',' ')
    bpmnumber    = range(len(x))
    labels       = Parameters.bpminfo.keys()[:len(x)]

    pltarea      = Plot.PlotAreaAvg(bpmnumber,x,np.zeros_like(bpmnumber),xstd)
    tv1          = plt.figure(figsize=(18,6))
    tv2          = plt.subplot(111)
    plt.errorbar(bpmnumber,x,yerr=xstd,label='X',fmt='r.',linestyle='-',mew=1) #red
    plt.errorbar(bpmnumber,y,yerr=ystd,label='Y',fmt='b.',linestyle='-',mew=1) #blue
    plt.axis(pltarea)
    plt.title(plttitle)
    plt.xlabel('BPM')
    plt.ylabel('Position ($\mu$m)')
    plt.legend()
    tv2.xaxis.set_major_locator(majorLocator)
    tv2.xaxis.set_minor_locator(minorLocator)
    plt.grid('true',which='minor')
    plt.grid('true',which='major')
    plt.xticks(bpmnumber,labels,rotation=90)
    tv1.set_figheight(6)
    tv1.set_figwidth(18)
    tv1.subplots_adjust(bottom=0.3)
        
    plotfn = savedir+filename+'_bpm_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    tv1.savefig(plotfn,dpi=300,format=None)
    tv1.savefig(twikfn)
    tv1.show(block=False)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 

def PlotHist(y,ylabel,nbins,savedir,filename) :
    plttitle  = filename.replace('_',' ')
    fig       = plt.figure()
    fig.text(0.5, 0.92,plttitle,horizontalalignment='center',size='large')
        
    plt.hist(y,bins=nbins,normed=True,histtype='bar',rwidth=0.8,align='mid',orientation='vertical',edgecolor='none',facecolor='b',label='Horizontal')
    plt.xlabel(ylabel)#swapped because of this type of plot
    plt.ylabel('Normalised Counts')
        
    plotfn = savedir+filename+'_y_hist.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 
    plt.show(block=False)

def PlotXYFitData(x,xstd,y,ystd,ymodel,signalmodel,xlabel,ylabel,fitparams,savedir,filename,tex=False) :
    """
    PlotXYFitData(x,xstd,y,ystd,ymodel,signalmodel,savedir,filename)
        
    Plots XY data + errorbars + fit ydata and saves to pdf to
    savedir/filename_xy_fit_plot.pdf 
    """
        
    plttitle      = filename.replace('_',' ')
    #ystd          = ErrorBarLimit(y,ystd,0,1e9)
    pltarea       = PlotAreaAvg(x,y,xstd,ystd)
    paramsloc     = PlotFitLegLoc(pltarea)
    
    plt.figure()
    plt.errorbar(x,y,xerr=xstd,yerr=ystd,fmt='o',linewidth=1,mew=1,mec='b',ms=3,color='b',label='Data',alpha=0.4)
    plt.plot(ymodel,signalmodel,color='g',label='Model',lw=2)
    plt.axis(pltarea)
    plt.title(plttitle)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    fittext = FitText(fitparams,tex)
    plt.text(paramsloc[0],paramsloc[1],fittext,bbox=dict(facecolor='white'),size='small',linespacing=1.6)
    plotfn = savedir+filename+'_xy_fit_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn
    plt.show(block=False)

def PlotXYErrorBar(x,xstd,y,ystd,xlabel,ylabel,savedir,filename) :
    """
    PlotXYErrorbar(x,xstd,y,ystd,xlabel,ylabel,savedir,filename)
    Plots XY data + errorbars and saves to pdf to

    savedir/filename_xy_errbar_plot.pdf
    """
    x = flatten(x)
    y = flatten(y)
    xstd = flatten(xstd)
    ystd = flatten(ystd)
    
    plttitle      = filename.replace('_',' ')
    #ystd          = ErrorBarLimit(y,ystd,0,1e9)
    #errorbars below zero are valid due to background subtract
    pltarea       = PlotAreaAvg(x,y,xstd,ystd)
    
    plt.figure()
    plt.errorbar(x,y,xerr=xstd,yerr=ystd,fmt='o',linewidth=1,mew=1,mec='b',ms=3,color='b',label='Data')
    plt.axis(pltarea)
    plt.title(plttitle)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plotfn = savedir+filename+'_xy_errbar_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None,alpha=0.6)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 
    plt.show(block=False)

def PlotXYDataLine(x,y,xlabel,ylabel,savedir,filename) :
    """
    PlotXYDataLine(x,y,xlabel,ylabel,savedir,filename)
    
    saves to savedir/filename_xy_line_plot.pdf
    """
    
    plttitle     = filename.replace('_',' ')
    pltarea      = PlotArea(x,y)
    
    plt.figure()
    plt.plot(x,y)
    plt.axis(pltarea)
    plt.title(plttitle)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plotfn = savedir+filename+'_xy_line_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn

def PlotXYData(x,y,xlabel,ylabel,savedir,filename) :
    """
    PlotXYData(x,y,xlabel,ylabel,savedir,filename)
    
    Plots XY data and saves to pdf in savedir/filename_xy_scatter_plot.pdf.
    """
    
    plttitle     = filename.replace('_',' ')
    pltarea      = PlotArea(x,y)
    
    plt.figure()
    plt.plot(x,y,'b.')
    plt.axis(pltarea)
    plt.title(plttitle)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plotfn = savedir+filename+'_xy_scatter_plot.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn
    plt.show(block=False)
        
def FitText(fitparamsdict,tex=False):
    fpd = fitparamsdict
    if tex == False:
        string = ''
        keys = np.sort(fpd.keys())
        for key in keys:
            if key != 'chi2red':
                substring = key+' = '+str(round(fpd[key][0],2))+'$\pm$'+str(round(fpd[key][1],2))+'\n'
                string = string+substring
        if fpd.has_key('chi2red'):
            if type(fpd['chi2red']) == list:
                chi2red = fpd['chi2red'][0]
            else:
                chi2red = fpd['chi2red']
            #substring = 'Chi2_red '+' : '+str(round(chi2red,2))
            substring = '$\chi_{red}^{2}$ '+' = '+str(round(chi2red,2))
            string = string+substring
    else:
        string = r"\usepackage{eqnarray}\begin{eqnarray*}\\"
        keys = np.sort(fpd.keys())
        for key in keys:
            if key != 'chi2red':
                substring = key+r"&= "+str(round(fpd[key][0],2))+r"$\pm$"+str(round(fpd[key][1],2))+r"\\"
                string = string+substring
        if fpd.has_key('chi2red'):
            if type(fpd['chi2red']) == list:
                chi2red = fpd['chi2red'][0]
            else:
                chi2red = fpd['chi2red']
        substring = r"$\chi_{red}$ "+r"&="+str(round(chi2red,2))
        string = string+substring+r"\end{eqnarray*}\end{document}"

    return string  

def PlotArea(x,y) :
    """        
    PlotArea(x,y)
    
    Use this function to return 'area' a list of xmin, xmax, ymin and ymax plot bounds.
    
    """
    xmin = np.min(x)
    xmax = np.max(x)
    xlen = xmax - xmin
    xmin = xmin - (Parameters.parameters['graphmarginfraction']*xlen)
    xmax = xmax + (Parameters.parameters['graphmarginfraction']*xlen)
    
    ymin = np.min(y)
    ymax = np.max(y)
    ylen = ymax - ymin
    ymin = ymin - (Parameters.parameters['graphmarginfraction']*ylen)
    ymax = ymax + (Parameters.parameters['graphmarginfraction']*ylen)
    
    area = [xmin,xmax,ymin,ymax]
    return area
        
def PlotAreaAvg(x,y,xerr,yerr) :
    """        
    PlotAreaAvg(x,y,xerr,yerr)
    
    Use this function to return 'area' a list of xmin, xmax, ymin and ymax plot bounds.
    """
        
    xmin = np.min(x-xerr)
    xmax = np.max(x+xerr)
    xlen = xmax - xmin
    xmina = xmin - (Parameters.parameters['graphmarginfraction']*xlen)
    xmaxa = xmax + (Parameters.parameters['graphmarginfraction']*xlen)
    
    ymin = np.min(y-yerr)
    ymax = np.max(y+yerr)
    ylen = ymax - ymin
    ymina = ymin - (Parameters.parameters['graphmarginfraction']*ylen)
    ymaxa = ymax + (Parameters.parameters['graphmarginfraction']*ylen)
    area = [xmina,xmaxa,ymina,ymaxa]
    return area

def PlotFitLegLoc(area) :
    xstart = area[0]
    xstop  = area[1]
    ystart = area[2]
    ystop  = area[3]
    xran   = xstop - xstart
    yran   = ystop - ystart
    xloc   = xstop - (xran*0.32)
    yloc   = ystart + (yran*0.48)
    loc    = [xloc,yloc]
    return loc

def ErrorBarLimit(data,datastd,lowerlimit,upperlimit) :
    """
    LowerErrorBarLimit(data,datastd,limit)
        
    Use this function to rule out unphysical error bars like 99% +- 5%.
    Returns replacement datastd array    
    """
    upperdatastd = datastd
    lowerdatastd = datastd
    upperdatastd[(data-datastd)<lowerlimit] = lowerlimit
    lowerdatastd[(data+datastd)>upperlimit] = upperlimit
    newdatastd = np.array([upperdatastd,lowerdatastd])

    return newdatastd

def PlotFocus(x,ystd,focuslocation):
        
    plt.figure()
    plt.plot(x,ystd)
    plt.axvline(focuslocation,color='r')

def RMS(x,y,savedir,filename):
    xf = flatten(x)
    yf = flatten(y)
    yback = np.mean([yf[:3].mean(),yf[-3:].mean()])
    yb1   = yf[:3].mean()
    yb2   = yf[-3:].mean()
    xb1   = xf[:3].mean()
    xb2   = xf[-3:].mean()
    gdt     = (yb2-yb1)/(xb2-xb1)
    ybgs    = y - (gdt*(x-xb1) + yb1)
    ybgpred = gdt*(x-xb1) + yb1
    
    plt.figure()
    plt.subplots_adjust(hspace=0.3,wspace=0.3)
    plt.suptitle('RMS Control Plot',size='xx-large')
    
    area = PlotArea(x,y)
    plt.subplot(2,2,1,autoscale_on=False,xlim=(area[0],area[1]),ylim=(area[2],area[3]))
    plt.axis(area)
    plt.title('Raw Data')
    plt.plot(x,y,'.')
    
    plt.subplot(2,2,2,autoscale_on=False,xlim=(area[0],area[1]),ylim=(area[2],area[3]))
    plt.title('Background Fit')
    plt.plot(x,y,'.')
    plt.plot(x,ybgpred,'-')
    
    area = PlotArea(x,ybgs)
    plt.subplot(2,2,3,autoscale_on=False,xlim=(area[0],area[1]),ylim=(area[2],area[3]))
    plt.title('Background Subtracted')
    plt.plot(x,ybgs)
    
    xb    = np.average(x,weights=ybgs)
    ybgs  = np.abs(ybgs)
    rms   = np.sqrt(np.average((x-xb)**2,weights=ybgs))
    wmin = xb-rms/2.
    wmax = xb+rms/2.
    rms = {'rms':rms,'centre':xb,'wmin':wmin,'wmax':wmax}
    
    loc = PlotFitLegLoc(area)
    
    plt.subplot(2,2,4,autoscale_on=False,xlim=(area[0],area[1]),ylim=(area[2],area[3]))
    plt.title('BGS $\pm$ RMS/2')
    plt.plot(x,ybgs)
    plt.axvline(xb)
    plt.axvline(wmin)
    plt.axvline(wmax)
    #s = 'RMS = '+str(round(rms['rms']))
    #plt.text(loc[0],loc[1],s,size='small')
    
    plotfn = savedir+filename+'_rms.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn
    
    print 'RMS: ',rms
    print 'x-bar: ',xb
    print 'w-min, w-max: ',wmin,' ',wmax   

def Plot2DFitD(hdict,vdict):
    fig = plt.figure()
    ax  = fig.add_subplot(111,projection='3d')
    h = hdict
    v = vdict
    
    #plot horizontal & vertical scan
    ax.scatter(h['x'],h['y'],h['signal'],'b.')
    ax.scatter(v['x'],v['y'],v['signal'],'r.')
    #plot horizontal & vertical fit
    ax.plot(h['x_model'],h['y_model'],h['signal_model'],'b',label='Horizontal')
    ax.plot(v['x_model'],v['y_model'],v['signal_model'],'r',label='Vertical')
    plt.legend(prop={'size':14})    

def Plot2DFitContour(hdict,vdict,surfacedict,anadir,xlabel=None,ylabel=None,zlabel=None):
    fig = plt.figure()
    ax  = fig.add_subplot(111,projection='3d')
    fig.subplots_adjust(left=-0.11,top=1.05,bottom=0.05,right=0.98)
    ax.view_init(elev=30.,azim=115)
    h = hdict
    v = vdict
    s = surfacedict

    #plot horizontal & vertical scan
    ax.plot3D(h['x'],h['y'],h['signal'],'k.')
    ax.plot3D(v['x'],v['y'],v['signal'],'k.')
    #3D errorbar cludge as no functiontionality for this
    for i in np.arange(len(h['x'])):
        ax.plot3D([h['x'][i],h['x'][i]],[h['y'][i],h['y'][i]],[h['signal'][i]-h['signal_std'][i],h['signal'][i]+h['signal_std'][i]],marker='_',color='k')
    for i in np.arange(len(v['x'])):
        ax.plot3D([v['x'][i],v['x'][i]],[v['y'][i],v['y'][i]],[v['signal'][i]-v['signal_std'][i],v['signal'][i]+v['signal_std'][i]],marker='_',color='k')
    #plot horizontal & vertical fit
    ax.plot3D(h['x_model'],h['y_model'],h['signal_model'],color='k',label='Horizontal')
    ax.plot3D(v['x_model'],v['y_model'],v['signal_model'],color='k',label='Vertical')
    dim = np.sqrt(len(s['signal']))
    x2d = s['x'].reshape((dim,dim))
    y2d = s['y'].reshape((dim,dim))
    z2d = s['signal'].reshape((dim,dim))
    #ax.plot_wireframe(s['x'],s['y'],s['signal'],color='k',alpha=0.2)
    #ax.plot_surface(s['x'],s['y'],s['signal'],color='k',alpha=0.2)
    ax.contour(x2d,y2d,z2d,12,colors='k',alpha=0.2)
    
    zl = zlabel.split()
    zlen = len(zl)/2
    zlabel2 = ' '.join(zl[:zlen])+'\n'+' '.join(zl[zlen:])

    if xlabel != None:
        ax.set_xlabel(xlabel,fontsize='medium')
    if ylabel != None:
        ax.set_ylabel(ylabel,fontsize='large')
    if zlabel != None:
        ax.set_zlabel(zlabel2,fontsize='x-large')
    #plt.legend(prop={'size':14})

    ax.tick_params(labelsize=15)
    filename = anadir.split('/')[-2]
    
    plotfn = anadir+filename+'_xy_fit_contour.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None,alpha=0.6)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 


def Plot2DSurfaceContour(surfacedict,anadir,ncontours=12,xlabel=None,ylabel=None,zlabel=None):
    fig = plt.figure()
    ax  = fig.add_subplot(111,projection='3d')
    fig.subplots_adjust(left=-0.11,top=1.05,bottom=0.05,right=0.98)
    ax.view_init(elev=30.,azim=115)
    s = surfacedict
    
    dim = np.sqrt(len(s['signal']))
    x2d = s['x'].reshape((dim,dim))
    y2d = s['y'].reshape((dim,dim))
    z2d = s['signal'].reshape((dim,dim))
    #ax.plot_wireframe(s['x'],s['y'],s['signal'],color='k',alpha=0.2)
    #ax.plot_surface(s['x'],s['y'],s['signal'],color='k',alpha=0.2)
    ax.contour(x2d,y2d,z2d,ncontours,colors='k',alpha=0.6)
    
    zl = zlabel.split()
    zlen = len(zl)/2
    zlabel2 = ' '.join(zl[:zlen])+'\n'+' '.join(zl[zlen:])

    if xlabel != None:
        ax.set_xlabel(xlabel,fontsize='small')
    if ylabel != None:
        ax.set_ylabel(ylabel,fontsize='small')
    if zlabel != None:
        ax.set_zlabel(zlabel2,fontsize='small')

    ax.tick_params(labelsize=15)
    filename = anadir.split('/')[-2]
    
    plotfn = anadir+filename+'_surface_contour.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None,alpha=0.6)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 

def Plot2DSurfaceWireFrame(surfacedict,anadir,xlabel=None,ylabel=None,zlabel=None):
    fig = plt.figure()
    ax  = fig.add_subplot(111,projection='3d')
    fig.subplots_adjust(left=-0.11,top=1.05,bottom=0.05,right=0.98)
    ax.view_init(elev=30.,azim=115)
    s = surfacedict
    
    dim = np.sqrt(len(s['signal']))
    x2d = s['x'].reshape((dim,dim))
    y2d = s['y'].reshape((dim,dim))
    z2d = s['signal'].reshape((dim,dim))
    ax.plot_wireframe(s['x'],s['y'],s['signal'],color='k',alpha=0.4)
    #ax.plot_surface(s['x'],s['y'],s['signal'],color='k',alpha=0.2)
    #ax.contour(x2d,y2d,z2d,ncontours,colors='k')
    
    zl = zlabel.split()
    zlen = len(zl)/2
    zlabel2 = ' '.join(zl[:zlen])+'\n'+' '.join(zl[zlen:])

    if xlabel != None:
        ax.set_xlabel(xlabel,fontsize='small')
    if ylabel != None:
        ax.set_ylabel(ylabel,fontsize='small')
    if zlabel != None:
        ax.set_zlabel(zlabel2,fontsize='small')

    ax.tick_params(labelsize=15)
    filename = anadir.split('/')[-2]
    
    plotfn = anadir+filename+'_surface_contour.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None,alpha=0.6)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 


def Plot2DFitFull(hdict,vdict,surfacedict,anadir,xlabel=None,ylabel=None,zlabel=None):
    #easy data names
    h = hdict
    v = vdict
    s = surfacedict
    
    #default figures size is 8x6 - use double width here
    fig = plt.figure(figsize=(14,6))
    #3d plot on left
    ax1  = fig.add_subplot(121,projection='3d')
    fig.subplots_adjust(left=-0.05,top=0.98,bottom=0.09,right=0.99,wspace=0.08,hspace=0.25)
    ax1.view_init(elev=30,azim=115)
    #to projections in subplots stacked on right
    ax2  = fig.add_subplot(222)
    ax3  = fig.add_subplot(224)

   
    #3D Plot:
    #plot horizontal & vertical scan
    ax1.plot3D(h['x'],h['y'],h['signal'],'k.')
    ax1.plot3D(v['x'],v['y'],v['signal'],'k.')
    #3D errorbar cludge as no functiontionality for this
    for i in np.arange(len(h['x'])):
        ax1.plot3D([h['x'][i],h['x'][i]],[h['y'][i],h['y'][i]],[h['signal'][i]-h['signal_std'][i],h['signal'][i]+h['signal_std'][i]],marker='_',color='k')
    for i in np.arange(len(v['x'])):
        ax1.plot3D([v['x'][i],v['x'][i]],[v['y'][i],v['y'][i]],[v['signal'][i]-v['signal_std'][i],v['signal'][i]+v['signal_std'][i]],marker='_',color='k')
    #plot horizontal & vertical fit
    ax1.plot3D(h['x_model'],h['y_model'],h['signal_model'],color='k',label='Horizontal')
    ax1.plot3D(v['x_model'],v['y_model'],v['signal_model'],color='k',label='Vertical')
    dim = np.sqrt(len(s['signal']))
    x2d = s['x'].reshape((dim,dim))
    y2d = s['y'].reshape((dim,dim))
    z2d = s['signal'].reshape((dim,dim))
    ax1.contour(x2d,y2d,z2d,12,colors='k',alpha=0.2)
    zl = zlabel.split()
    zlen = len(zl)/2
    zlabel2 = ' '.join(zl[:zlen])+'\n'+' '.join(zl[zlen:])
    if xlabel != None:
        ax1.set_xlabel(xlabel,fontsize='small')
    if ylabel != None:
        ax1.set_ylabel(ylabel,fontsize='small')
    if zlabel != None:
        ax1.set_zlabel(zlabel2,fontsize='small')
    ax1.tick_params(labelsize=15)


    #Horizontal Projection
    x    = flatten(h['x'])
    xstd = flatten(h['xstd'])
    y    = flatten(h['signal'])
    ystd = flatten(h['signal_std'])
    xm   = flatten(h['x_model'])
    sm   = flatten(h['signal_model'])
    pltarea       = PlotArea(xm,sm)
    ax2.errorbar(x,y,xerr=xstd,yerr=ystd,fmt='.',linewidth=1,mew=1,mec='k',ms=3,color='k')
    ax2.plot(xm,sm,'k',alpha=0.6)
    ax2.grid()
    ax2.axis(pltarea)
    ax2.set_xlabel(xlabel,fontsize='small')

    #Vertical Projection
    x    = flatten(v['y'])
    xstd = flatten(v['ystd'])
    y    = flatten(v['signal'])
    ystd = flatten(v['signal_std'])
    ym   = flatten(v['y_model'])
    sm   = flatten(v['signal_model'])
    pltarea       = PlotArea(ym,sm)
    ax3.errorbar(x,y,xerr=xstd,yerr=ystd,fmt='.',linewidth=1,mew=1,mec='k',ms=3,color='k')
    ax3.plot(ym,sm,'k',alpha=0.6)
    ax3.grid()
    ax3.axis(pltarea)
    ax3.set_xlabel(ylabel,fontsize='small')

    #fig.tight_layout()
    filename = anadir.split('/')[-2]
    
    plotfn = anadir+filename+'_2d_fit_full.pdf'
    plotfn = General.CheckFileExists(plotfn)
    twikfn = plotfn[:-4]+'.png'
    plt.savefig(plotfn,dpi=300,format=None,alpha=0.6)
    plt.savefig(twikfn)
    print 'Plot saved to '+plotfn
    print 'Plot saved to '+twikfn 


def PlotEBeamFocus(ebeamfocusdict):
    d = ebeamfocusdict

    plt.figure()
