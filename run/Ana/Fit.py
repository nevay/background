import minuit
import numpy as np
import os
import subprocess
import shutil

import General

localsettings = General.GetDirs()
printmode     = int(localsettings['printmode'][0])
rundir        = localsettings['rundir']

def GeneralisedLogistic(x,A,K,B,v,Q,M):
    return A+((K-A)/((1+Q*np.exp(-B*(x-M)))**(1/v)))

def NormalisedLorentzian(x,offset,amplitude,xo,gamma):
    a = (1./np.pi)*(gamma/((x-xo)**2 + gamma**2))
    an = a/np.max(a)
    return offset + amplitude*an

def FitParamsInit(x,y,model):
    if model == 'Pol0' :
        offset          = np.mean(y)
        fpi             = {'offset':offset}
            
    elif model == 'Pol1' :
        offset          = np.min(y)
        gradient        = (np.max(y)-np.min(y))/(np.max(x)-np.min(x))
        fpi             = {'offset':offset, 'gradient':gradient}
    elif model == 'Pol2' :
        a               = (np.max(y)-np.min(y))/100.
        b               = 0.0
        c               = np.min(y)
        centre          = np.average(x,weights=1.0/(y**4.0))
        fpi             = {'a':a,'b':b,'c':c,'centre':centre}
    elif model == 'Pol6':
        a0              = np.min(y)
        a1              = 1
        a2              = 1
        a3              = 0.000001
        a4              = 0.000001
        a5              = 0.000001
        a6              = 0.000001
        x0              = np.min(x)
        fpi             = {'a0':a0,'a1':a1,'a2':a2,'a3':a3,'a4':a4,'a5':a5,'a6':a6,'x0':x0}
    elif model == 'Gauss':
        amplitude       = np.max(y)-np.min(y)
        centre          = np.average(x,weightsy)
        rms             = np.sqrt(np.average((x-centre)**2,weights=y))
        #sigma           = (np.max(x)-np.min(x))*0.08
        fpi             = {'centre':centre, 'sigma':rms, 'amplitude':amplitude}
    elif model == 'GaussPlusPol0' : 
        amplitude       = np.max(y)-np.min(y)
        offset          = np.min(y)
        #centre          = self.x[np.argmax(y)]
        centre          = np.average(x,weights=y)
        sigma           = (np.max(x)-np.min(x))*0.08
        fpi             = {'centre':centre, 'sigma':sigma, 'amplitude':amplitude, 'offset':offset}
    elif model == 'GaussPlusPol1' :
        amplitude       = np.max(y)-np.min(y)
        offset          = np.min(y)
        centre          = x[np.argmax(y)]
        sigma           = (np.max(x)-np.min(x))*0.1
        gradient        = (np.max(y)-np.min(y))/(np.max(x)-np.min(x))
        fpi             = {'centre':centre, 'sigma':sigma, 'amplitude':amplitude, 'offset':offset, 'gradient':gradient}
    elif model == 'Logistic':
        o_i  = np.min(y)
        c_i  = np.max(y)
        B_i  = 0.01
        v_i  = 20.0
        M_i  = 0.1 #np.mean(x)
        fpi  = {'offset':o_i,'ceiling':c_i,'B':B_i,'v':v_i,'M':M_i}
    elif model == 'Photodiode':
        a_i  = 0.0
        b_i  = 0.0035
        m_i  = 0.01
        xo_i = 0.0
        fpi  = {'a':a_i,'b':b_i,'m':m_i,'xo':xo_i}

    elif model == 'PhotodiodeInverse':
        a_i  = np.max(y)-np.min(y)
        b_i  = 0.03
        c_i  = 0.0
        m_i  = 1.0
        fpi  = {'a':a_i,'b':b_i,'c':c_i,'m':m_i,}

    elif model == 'Saturation':
        offset   = np.min(y)
        gradient = ( np.max(y) - np.min(y) ) / ( np.max(x) - np.min(x) )
        x0       = 1.0
        x1       = (np.max(x) - np.min(x) )/2.0
        s        = 20.0
        fpi  = {'offset':offset,'gradient':gradient,'x0':x0,'x1':x1,'s':s}
    
    elif model == 'Lorentzian':
        offset    = np.min(y)
        amplitude = np.max(y) - np.min(y)
        xo        = np.average(x,weights=y**3)
        gamma     = np.sqrt(np.average((x-xo)**2,weights=y))#rms
        fpi       = {'o':offset,'a':amplitude,'xo':xo,'g':gamma}
    
    elif model == 'Emittance':
        a         = (np.max(y)-np.min(y))/100.
        b         = np.average(x,weights=1.0/(y**4.0))
        c         = np.min(y)
        fpi       = {'a':a,'b':b,'c':c}

    elif model == 'EmittanceThick':
        a         = (np.max(y)-np.min(y))*40.
        b         = np.average(x,weights=1.0/(y**4.0))-1.0
        c         = np.min(y)*2.5
        fpi       = {'a':a,'b':b,'c':c}
        
    else :
        amplitude       = np.max(y)-np.min(y)
        offset          = np.min(y)
        centre          = np.mean(x)
        sigma           = (np.max(x)-np.min(x))*0.1
        fpi             = {'centre':centre, 'sigma':sigma, 'amplitude':amplitude, 'offset':offset}
            
    print 'Initial parameters\n',fpi,'\n'
    return fpi

def Fit(x,y,ystd,model,direction=None) :
    """
    Fit(x,y,ystd,model,direction=None)
    
    model is one of
    'Pol0'
    'Pol1'
    'Pol2'
    'Pol6'
    'GaussPlusPol0'
    'GaussPlusPol1'
    'ManVer'
    'Logistic'
    'Photodiode'
    'Saturation'
    'Lorentzian'
    'Emittance'
    'EmittanceThick'

    Direction can be 'pos' or 'neg' or unspecified in which cas positive assumed
    to limit amplitude to be positive or negative
    
    Returns:
    fitparams - dictionary of final fitted parameters
    ymodel    - ydata for model using final fitted parameters
    xmodel    - xdata for model
    """
    if not model in ['Pol0','Pol1','Pol2','Pol6','Gauss','GaussPlusPol0','Logistic','Photodiode','PhotodiodeInverse','Saturation','Lorentzian','Emittance','EmittanceThick']:
        print 'lwAna.Fit> Invalid model'
        return
    
    fpi = FitParamsInit(x,y,model)
    #invert the intial amplitude guess if 'negative' fit
    if direction == 'neg':
        if fpi.has_key('amplitude'):
            fpi['amplitude'] = -1*fpi['amplitude']
    
    if model == 'Pol0' :
        fnc  = lambda offset : offset*np.ones(np.shape(x))
        fnc2 = lambda x2,offset : offset*np.ones(np.shape(x2))
        chi2 = lambda offset : ((fnc(offset)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,offset=fpi['offset'])
        m.tol = 0.1
    elif model == 'Pol1' :
        fnc  = lambda offset, gradient : offset + x*gradient
        fnc2 = lambda x2,offset, gradient : offset + x2*gradient 
        chi2 = lambda offset, gradient : ((fnc(offset,gradient)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,offset=fpi['offset'],gradient=fpi['gradient'])
        m.tol = 0.1
    elif model == 'Pol2':
        fnc  = lambda a,b,c,centre: a*((x-centre)**2) + b*(x-centre) + c
        fnc2 = lambda x2,a,b,c,centre: a*((x2-centre)**2) + b*(x2-centre) + c
        chi2 = lambda a,b,c,centre : ((fnc(a,b,c,centre)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,a=fpi['a'],b=fpi['b'],c=fpi['c'],centre=fpi['centre'])
        m.limits['c'] = (0,100000)
    elif model == 'Pol6' :
        fnc  = lambda a0, a1, a2, a3, a4, a5, a6, x0 : a0+a1*(x-x0)+a2*(x-x0)**2+a3*(x-x0)**3+a4*(x-x0)**4+a5*(x-x0)**5+a6*(x-x0)**6
        fnc2 = lambda x2,a0, a1, a2, a3, a4, a5, a6, x0 : a0+a1*(x2-x0)+a2*(x2-x0)**2+a3*(x2-x0)**3+a4*(x2-x0)**4+a5*(x2-x0)**5+a6*(x2-x0)**6
        chi2 = lambda a0, a1, a2, a3, a4, a5, a6, x0 : ((fnc(a0,a1,a2,a3,a4,a5,a6,x0)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,a0=fpi['a0'],a1=fpi['a1'],a2=fpi['a2'],a3=fpi['a3'],a4=fpi['a4'],a5=fpi['a5'],a6=fpi['a6'],x0=fpi['x0'])
        m.tol = 0.1
    elif model == 'Gauss' :
        fnc  = lambda centre,sigma,amplitude, : amplitude*np.exp((-0.5)*(((x - centre)/sigma)**2))
        fnc2 = lambda x2,centre,sigma,amplitude, : amplitude*np.exp((-0.5)*(((x2 - centre)/sigma)**2))
        chi2 = lambda centre,sigma,amplitude, : ((fnc(centre,sigma,amplitude)-y)**2/ystd**2).sum()
        if direction == 'neg':
            m    = minuit.Minuit(chi2,centre=fpi['centre'],sigma=fpi['sigma'],amplitude=fpi['amplitude'],limit_amplitude=(-1000000,0),limit_sigma=(0.1,100000000))
        else:
            m    = minuit.Minuit(chi2,centre=fpi['centre'],sigma=fpi['sigma'],amplitude=fpi['amplitude'],limit_amplitude=(0,1000000),limit_sigma=(0.1,100000000))
        m.tol = 0.1
    elif model == 'GaussPlusPol0' :
        fnc  = lambda centre,sigma,amplitude,offset : offset + amplitude*np.exp((-0.5)*(((x - centre)/sigma)**2))
        fnc2 = lambda x2,centre,sigma,amplitude,offset : offset + amplitude*np.exp((-0.5)*(((x2 - centre)/sigma)**2))
        chi2 = lambda centre,sigma,amplitude,offset : ((fnc(centre,sigma,amplitude,offset)-y)**2/ystd**2).sum()
        if direction == 'neg':
            m    = minuit.Minuit(chi2,centre=fpi['centre'],sigma=fpi['sigma'],amplitude=fpi['amplitude'],offset=fpi['offset'],limit_amplitude=(-100000,0),limit_sigma=(0,800))
        else:
            m    = minuit.Minuit(chi2,centre=fpi['centre'],sigma=fpi['sigma'],amplitude=fpi['amplitude'],offset=fpi['offset'],limit_amplitude=(0,100000),limit_sigma=(0,800))
        m.tol = 0.1

    elif model == 'Logistic':
        fnc  = lambda offset,ceiling,B,v,M: GeneralisedLogistic(x,offset,ceiling,B,v,20.0,M)
        fnc2 = lambda x2,offset,ceiling,B,v,M: GeneralisedLogistic(x2,offset,ceiling,B,v,20.0,M)
        chi2 = lambda offset,ceiling,B,v,M: ((fnc(offset,ceiling,B,v,M)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,offset=fpi['offset'],ceiling=fpi['ceiling'],B=fpi['B'],v=fpi['v'],M=fpi['M'],fix_v=True) 
        m.errors['offset']  = 0.01
        m.errors['ceiling'] = 0.01
        m.errors['B']       = 0.05
        m.errors['v']       = 1.0
        m.errors['M']       = 10.0
        m.limits['offset']  = (-0.5,16000)
        m.limits['ceiling'] = (0,16000)
        m.limits['B']       = (0.0000001,0.5)
        m.limits['v']       = (0,40)
        m.tol               = 100
        m.up                = 10

    elif model == 'Photodiode':
        fnc  = lambda a,b,m,xo: a + np.exp(b*(x-xo)) + (m*(x-xo))
        fnc  = lambda x2,a,b,m,xo: a + np.exp(b*(x2-xo)) + (m*(x2-xo))
        chi2 = lambda a,b,m,xo: ((fnc(a,b,m,xo)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,a=fpi['a'],b=fpi['b'],m=fpi['m'],xo=fpi['xo'])
        m.tol       = 0.1
        
    elif model == 'PhotodiodeInverse':
        fnc  = lambda a,b,c,m: (2*a / (1+np.exp(-1*b*x)))+c-a+(m*x)
        fnc2 = lambda x2,a,b,c,m: (2*a / (1+np.exp(-1*b*x2)))+c-a+(m*x2)
        chi2 = lambda a,b,c,m: ((fnc(a,b,c,m)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,a=fpi['a'],b=fpi['b'],c=fpi['c'],m=fpi['m'])
        m.tol       = 0.1
        
    elif model == 'Saturation':
        fnc  = lambda offset,gradient,x0,x1,s: offset + gradient*(x-x0) - (1./(1.+np.exp(-(x-x1)/s)))*gradient*(x-x1)
        fnc2 = lambda x2,offset,gradient,x0,x1,s: offset + gradient*(x2-x0) - (1./(1.+np.exp(-(x2-x1)/s)))*gradient*(x2-x1)
        chi2 = lambda offset,gradient,x0,x1,s: (((fnc(offset,gradient,x0,x1,s)-y)**2)/(ystd**2)).sum()
        m    = minuit.Minuit(chi2,o=fpi['offset'],m=fpi['gradient'],a=fpi['x0'],b=fpi['x1'],x0=fpi['s'])
        m.limits['offset']   = (-2.0,5.0)
        m.limits['gradient'] = (0,1.0)
        m.limits['x0']       = (-5,5)
        m.limits['x1']       = (40,100)
        m.limits['s']        = (10,30.0)
        m.tol  = 10
    
    elif model == 'Lorentzian':
        fnc  = lambda o,a,xo,g:  NormalisedLorentzian(x,o,a,xo,g)
        fnc2 = lambda x2,o,a,xo,g:  NormalisedLorentzian(x2,o,a,xo,g)
        chi2 = lambda o,a,xo,g: ((fnc(o,a,xo,g)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,o=fpi['o'],a=fpi['a'],xo=fpi['xo'],g=fpi['g'])
        m.limits['g'] = (0.1,100)
        m.tol = 0.1

    elif model == 'Emittance':
        fnc  = lambda a,b,c: a*(x-b)**2 + c
        fnc2 = lambda x2,a,b,c: a*(x2-b)**2 + c
        chi2 = lambda a,b,c: ((fnc(a,b,c)-y)**2/ystd**2).sum()
        m    = minuit.Minuit(chi2,a=fpi['a'],b=fpi['b'],c=fpi['a'])
    
    elif model == 'EmittanceThick':
        from lwEmittance import m11
        from lwEmittance import m12
        fnc   = lambda a,b,c:  a*((m11(x) + b*m12(x))**2) + c*(m12(x)**2)
        fnc2  = lambda x2,a,b,c: a*((m11(x2) + b*m12(x2))**2) + c*(m12(x2)**2)
        chi2  = lambda a,b,c: ((fnc(a,b,c)-y)**2/ystd**2).sum()
        m     = minuit.Minuit(chi2,a=fpi['a'],b=fpi['b'],c=fpi['c'])
        #m.limits['a'] = (0,1e5)
        #m.limits['a'] = (0,1e-8)

    m.maxcalls     = 10000    
    m.strategy     = 1
    m.printMode    = 1

    m.migrad()
    m.hesse()
        
    fitparams = {} # each value has array([parameter,uncertainty])
    for key in m.values:
        fitparams[key] = [m.values[key],m.errors[key]]
    ps              = np.array(fitparams.values())[:,0]
    fp              = fitparams


    #generate more detail x values for model
    npoints = 500
    stepsize = (( np.max(x) - np.min(x) ) / npoints)
    xmodel  = np.arange(np.min(x),np.max(x),stepsize)

    if model == 'Pol0':
        ymodel = fnc2(xmodel,fp['offset'][0])
        fchi2  = chi2(fp['offset'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-1.0-1.0)]
    elif model == 'Pol1':
        ymodel = fnc2(xmodel,fp['offset'][0],fp['gradient'][0])
        fchi2  = chi2(fp['offset'][0],fp['gradient'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-2.0-1.0)]
    elif model == 'Pol2':
        ymodel = fnc2(xmodel,fp['a'][0],fp['b'][0],fp['c'][0],fp['centre'][0])
        fchi2  = chi2(fp['a'][0],fp['b'][0],fp['c'][0],fp['centre'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'Pol6':
        ymodel = fnc2(xmodel,fp['a0'][0],fp['a1'][0],fp['a2'][0],fp['a3'][0],fp['a4'][0],fp['a5'][0],fp['a6'][0],fp['x0'][0])
        fchi2  = chi2(fp['a0'][0],fp['a1'][0],fp['a2'][0],fp['a3'][0],fp['a4'][0],fp['a5'][0],fp['a6'][0],fp['x0'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-7.0-1.0)]
    elif model == 'GaussPlusPol0':
        ymodel = fnc2(xmodel,fp['centre'][0],fp['sigma'][0],fp['amplitude'][0],fp['offset'][0])
        fchi2  = chi2(fp['centre'][0],fp['sigma'][0],fp['amplitude'][0],fp['offset'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'Gauss':
        ymodel = fnc2(xmodel,fp['centre'][0],fp['sigma'][0],fp['amplitude'][0])
        fchi2  = chi2(fp['centre'][0],fp['sigma'][0],fp['amplitude'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-3.0-1.0)]
    elif model == 'Logistic':
        ymodel = fnc2(xmodel,fp['offset'][0],fp['ceiling'][0],fp['B'][0],fp['v'][0],fp['M'][0])
        fchi2  = chi2(fp['offset'][0],fp['ceiling'][0],fp['B'][0],fp['v'][0],fp['M'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-6.0-1.0)]
    elif model == 'Photodiode':
        ymodel = fnc2(xmodel,fp['a'][0],fp['b'][0],fp['m'][0],fp['xo'][0])
        fchi2  = chi2(fp['a'][0],fp['b'][0],fp['m'][0],fp['xo'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'PhotodiodeInverse':
        ymodel = fnc2(xmodel,fp['a'][0],fp['b'][0],fp['c'][0],fp['m'][0])
        fchi2  = chi2(fp['a'][0],fp['b'][0],fp['c'][0],fp['m'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'Saturation':
        ymodel = fnc2(xmodel,fp['offset'][0],fp['gradient'][0],fp['x0'][0],fp['x1'][0],fp['s'][0])
        fchi2  = chi2(fp['offset'][0],fp['gradient'][0],fp['x0'][0],fp['x1'][0],fp['s'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'Lorentzian':
        ymodel = fnc2(xmodel,fp['o'][0],fp['a'][0],fp['xo'][0],fp['g'][0])
        fchi2  = chi2(fp['o'][0],fp['a'][0],fp['xo'][0],fp['g'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-4.0-1.0)]
    elif model == 'Emittance':
        ymodel = fnc2(xmodel,fp['a'][0],fp['b'][0],fp['c'][0])
        fchi2  = chi2(fp['a'][0],fp['b'][0],fp['c'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-3.0-1.0)]
    elif model == 'EmittanceThick':
        ymodel = fnc2(xmodel,fp['a'][0],fp['b'][0],fp['c'][0])
        fchi2  = chi2(fp['a'][0],fp['b'][0],fp['c'][0])
        fitparams['chi2red'] = [fchi2/(len(y)-3.0-1.0)]   
    return fitparams,ymodel,xmodel
