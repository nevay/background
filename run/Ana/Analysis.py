import numpy as np
import AnaData, General, Orbit, Parameters, Plot, Routines
#import Fit # no minuit installed
from Routines import *

class Analysis(AnaData.AnaData):
    def __init__(self,filename):
        self.parameters    = Parameters.parameters
        self.parameterinfo = Parameters.parameterinfo
        AnaData.AnaData.__init__(self,filename)
        
        #specify dirs
        #self.rundir      = Data.Data.rundir
        #self.anadir      = Data.Data.anadir
        #self.datdir      = Data.Data.datdir
        
        self.debug       = False

    def FitGauss(self):
        self.fitparams,self.signal_model,self.y_model = Fit.FitGauss(self.x,self.y,self.ystd,self.filename,self.anadir)

    def Fit(self,model,direction=None):
        """Fits current X Y Data to model
        
        Usage:
        Fit(model)
        
        model is one of
        
        'Pol0'
        'Pol1'
        'GaussPlusPol0'
        'GaussPlusPol1'
        
        Requires YData to have been run already.
        
        Returns:
        self.fitparams    - dictionary of final fitted parameters
        self.signal_model - ydata for model using final fitted parameters
        self.y_model      - xdata for model using final fitted parameters
        
        self.ymodel generates its own (500) x data points.        
        
        """
        self.fitparams,self.signal_model,self.y_model = Fit.Fit(self.x,self.y,self.ystd,model,direction)

    def ControlPlot(self):
        Plot.ControlPlot(self.raw,self.datainfo,self.anadir,self.filename)

    def Correlation(self,x=None,y=None):
        if x == None or y == None:
            return General.Correlation(self.x,self.y)
        else:
            return General.Correlation(x,y)

    def PlotBpmCorrelation(self):
        atfxpos   = a.f['atf2xpos']
        atfypos   = a.f['atf2ypos']
        cherenkov = a.f['cherenkov']
        
        Plot.PlotBpmCorrelation(atf2xpos,atf2ypos,cherenkov,self.anadir,self.filename)

    def PlotCorrelation(self):
        Plot.PlotCorrelation(self.f,self.anadir,self.filename)

    def PlotCCL(self):
        Plot.PlotCCL(self.f,self.anadir,self.filename)

    def PlotBpmXYHist(self,bpmname):
        Plot.PlotBpmXYHist(self.x,self.y,self.anadir,self.filename,bpmname)

    def PlotBpmGlobal(self):
        self.X('cbpmx','f')
        self.Y('cbpmy','f')
        xs   = np.shape(self.x)
        ys   = np.shape(self.y)
        x    = np.reshape(self.x,((xs[0]*xs[1]),xs[2]))
        y    = np.reshape(self.y,((ys[0]*ys[1]),ys[2]))
        xstd = np.reshape(self.xstd,((np.shape(self.xstd)[0]*np.shape(self.xstd)[0]),np.shape(self.xstd)[2]))
        ystd = np.reshape(self.ystd,((np.shape(self.ystd)[0]*np.shape(self.ystd)[0]),np.shape(self.ystd)[2]))
        Plot.PlotBpmGlobal(x,xstd,y,ystd,self.anadir,self.filename)

    def PlotHist(self,nbins):
        Plot.PlotHist(self.y,self.ylabel,nbins,self.anadir,self.filename)

    def PlotXYFitData(self,tex=False):
        Plot.PlotXYFitData(self.x,self.xstd,self.y,self.ystd,self.y_model,self.signal_model,self.xlabel,self.ylabel,self.fitparams,self.anadir,self.filename,tex)

    def PlotXYErrorBar(self):
        Plot.PlotXYErrorBar(self.x,self.xstd,self.y,self.ystd,self.xlabel,self.ylabel,self.anadir,self.filename)

    def PlotXYDataLine(self):
        Plot.PlotXYDataLine(self.x,self.y,self.xlabel,self.ylabel,self.anadir,self.filename)

    def PlotXYData(self):
        Plot.PlotXYData(self.x,self.y,self.xlabel,self.ylabel,self.anadir,self.filename)

    def RMS(self):
        Plot.RMS(self.x,self.y,self.anadir,self.filename)

    def PlotEBeamFocus(self):
        Plot.PlotEBeamFocus(self.ebeamfocus)
        

