import numpy as np
import Data, Fit, General, Orbit, Parameters, Plot, Routines, Temp, Laser
from Routines import *
from Analysis import Analysis

class HVAnalysis:
    """
    Dual analysis of horizontal and vertical files together
    
    Coordinates:
    in each individual scan file, data is x and y but when dealing
    with different scans, they're referred to by their real axis in space
    'x' - horizontal lw
    'y' - vertical lw
    'signal' - detector signal
    'z' - ebeam axis (not used in scans)

    - HVAnalysis(horizontalfilepath,verticalfilepath)
    - BackgroundSubtract (optional) but always before filtering!!!
    - Filters... (optional)    
    - SelectSignal(keystring)
    
    where keystring is one of 
    cherenkovcn, cherenkovcnbgs, cherenkovcnbgslen, cherenkovcnbgslpn
    
    - FitOI2D
    - Plot...

    """
    def __init__(self,horizontalfilepath,verticalfilepath):
        self.h      = Analysis(horizontalfilepath)
        self.v      = Analysis(verticalfilepath)
        self.anadir = Data.DualAnaDir(self.h.filename,self.v.filename)
        self._UpdateX()

    def _UpdateX(self):
        self.h.X('chahor','a')
        self.v.X('chaver','a')

    def FilterAboveLimit(self,key,upperlimit=None):
        self.h.FilterAboveLimit(key,upperlimit)
        self.v.FilterAboveLimit(key,upperlimit)
        self._UpdateX()

    def FilterBelowLimit(self,key,lowerlimit=None):
        self.h.FilterBelowLimit(key,lowerlimit)
        self.v.FilterBelowLimit(key,lowerlimit)
        self._UpdateX()
        
    def BackgroundSubtract(self,backgroundlevel):
        self.h._NewBackgroundLevel(backgroundlevel)
        self.v._NewBackgroundLevel(backgroundlevel)

    def SelectSignal(self,key):
        self.h.Y(key,'a')
        self.v.Y(key,'a')
        
    def FitOI2D(self,makesurface=True,systematics=False,nsystematics=100):
        #put data together
        h_vposition = np.mean(self.h['chaver','a'])
        v_hposition = np.mean(self.v['chahor','a'])

        hlen = len(self.h.x)
        vlen = len(self.v.x)

        htotal     = np.concatenate((self.h.x,np.ones_like(self.v.x)*v_hposition))
        vtotal     = np.concatenate((np.ones_like(self.h.x)*h_vposition,self.v.x))
        stotal     = np.concatenate((self.h.y,self.v.y))
        sstdtotal  = np.concatenate((self.h.ystd,self.v.ystd))

        #fit
        if systematics == False:
            fitparams,dataout,surfacedata = Fit.FitOI2D(htotal,vtotal,stotal,sstdtotal,self.h.filename,self.anadir,makesurface)
        else:
            fitparams,dataout,surfacedata,rawsystematics,systematics = Fit.FitOI2DSystematics(htotal,vtotal,stotal,sstdtotal,self.h.filename,self.anadir,nsystematics)
            self.rawsystematics = rawsystematics
            self.systematics    = systematics
        self.fitdata        = dataout
        self.surfacedata    = surfacedata
        
        
        #read and split data
        #remember model data has more points than sample data
        #defined in oi cpp source as 500
        do = dataout
        splitpoint = len(do['x_model'])/2
        self.hdata = {}
        self.vdata = {}
        self.hdata['x']            = do['x'][:hlen]
        self.hdata['y']            = do['y'][:hlen]
        self.hdata['signal']       = do['signal'][:hlen]
        self.hdata['signal_std']   = do['signal_err'][:hlen]
        self.hdata['x_model']      = do['x_model'][:splitpoint]
        self.hdata['y_model']      = do['y_model'][:splitpoint]
        self.hdata['signal_model'] = do['signal_model'][:splitpoint]
        self.hdata['xstd']         = self.h.xstd
        self.hdata['ystd']         = np.ones_like(self.hdata['y'])*self.h.parameterinfo['chahor'][1]

        self.vdata['x']            = do['x'][hlen:]
        self.vdata['y']            = do['y'][hlen:]
        self.vdata['signal']       = do['signal'][hlen:]
        self.vdata['signal_std']   = do['signal_err'][hlen:]
        self.vdata['x_model']      = do['x_model'][splitpoint:]
        self.vdata['y_model']      = do['y_model'][splitpoint:]
        self.vdata['signal_model'] = do['signal_model'][splitpoint:]
        self.vdata['xstd']         = np.ones_like(self.vdata['y'])*self.v.parameterinfo['chaver'][1]
        self.vdata['ystd']         = self.v.xstd
        
        self.fitparams = fitparams

    def Plot2DFit(self):
        Plot.Plot2DFitContour(self.hdata,self.vdata,self.surfacedata,self.anadir,self.h.xlabel,self.v.xlabel,self.h.ylabel)

    def Plot2DFitNoSurface(self):
        Plot.Plot2DFit(self.hdata,self.vdata)
        
    def Plot2DSurfaceContour(self,ncontours=12):
        Plot.Plot2DSurfaceContour(self.surfacedata,self.anadir,ncontours,self.h.xlabel,self.v.xlabel,self.h.ylabel)
        
    def Plot2DSurfaceWireFrame(self):
        Plot.Plot2DSurfaceWireFrame(self.surfacedata,self.anadir,self.h.xlabel,self.v.xlabel,self.h.ylabel)

    def Plot2DFitFull(self):
        Plot.Plot2DFitFull(self.hdata,self.vdata,self.surfacedata,self.anadir,self.h.xlabel,self.v.xlabel,self.h.ylabel)
        

        
