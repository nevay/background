import Ana
import Data
import numpy as np

localsettings = Data.GetLocalSettings()

def Default(filepath,yvar):
    a = Ana.Analysis(filepath)
    a.FilterBelowLimit('refc1amp',100)
    a.FilterBelowLimit('cherenkov',100)
    xvar = a.datainfo['inforaw'][0]
    a.X(xvar,'a')                   
    a.Y(yvar,'a')
    a.PlotXYErrorBar()
    #a.ControlPlot()

def DefaultHorizontal(filepath):
    a = Ana.Analysis(filepath)
    a.FilterBelowLimit('refc1amp',100)
    a.FilterBelowLimit('cherenkov',100)
    a.X('chahor','a')
    a.Y('cherenkovcn','a')
    a.PlotXYErrorBar()
    a.Fit('GaussPlusPol0')
    a.PlotXYFitData()
            
def DefaultFit(filepath,yvar,fitmodel='OI',sex=235.0,dx=0.0):
    a = Ana.Analysis(filepath)
    a.FilterBelowLimit('refc1amp',100)
    a.FilterBelowLimit('cherenkov',100)
    xvar = a.datainfo['inforaw'][0]
    a.X(xvar,'a')                                         
    a.Y(yvar,'a')
    a.PlotXYErrorBar()
    if fitmodel == 'OI':
        a.FitOI(sex,dx)
    elif fitmodel == 'M2':
        a.FitM2(sex,dx)
    elif fitmodel == 'Gauss':
        a.FitGauss()
    else:
        a.Fit(fitmodel)
    a.PlotXYFitData()
    a.RMS() 
    #a.ControlPlot()
    

