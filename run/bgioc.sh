#!/bin/sh 
# start the bg ioc

case "$1" in
    start)
    echo "Starting $0"

    if [ -f ~/background/tmp/bgioc.lock ]; then
	echo "bgioc alread running"
	exit 0;
    fi

    cd ../ioc/iocBoot/iocbg/
    screen -d -m -S bgioc -h 1000 ./st.cmd
    touch ~/background/tmp/bgioc.lock
    echo $USER >> ~/background/tmp/bgioc.lock
    chmod ug+wr ~/background/tmp/bgioc.lock
    ;;

    stop)
    echo "Stopping $0"
    pid=`ps aux | grep -i "screen -d -m -S bgioc -h 1000 ./st.cmd" | grep -v grep | awk '{print $2;}'`
    echo "Killing pid: $pid"
    kill $pid
    rm -rf ~/background/tmp/bgioc.lock
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;
esac
