#!/bin/sh 
# start bg process

case "$1" in
    start)
    echo "Starting $0"

    if [ -f ~/background/tmp/bgproc.lock ]; then
	echo "iocproc alread running"
	exit 0;
    fi

    screen -d -m -S bgpyproc -h 1000 ./bgProcess.py    
    touch ~/background/tmp/bgproc.lock
    echo $USER >> ~/background/tmp/bgproc.lock
    chmod ug+wr ~/background/tmp/bgproc.lock
    ;;

    stop)
    echo "Stopping $0"
    pid=`ps aux | grep -i "screen -d -m -S bgpyproc -h 1000 ./bgProcess.py" | grep -v grep | awk '{print $2;}'`
    echo "Killing pid: $pid"
    kill $pid
    rm -rf ~/background/tmp/bgproc.lock
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;
esac
