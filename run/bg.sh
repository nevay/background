#!/bin/sh 
# control bg ioc

case "$1" in
    start)
    echo "Starting $0"

    ./bgioc.sh start
    ./bgStartup.py -a
    wait
    ./bgproc.sh start
    ;;

    stop)
    echo "Stopping $0"
    
    ./bgStartup.py -b    
    ./bgproc.sh stop
    ./bgioc.sh stop
    ;;

    restart)
    echo "Restarting $0"
    sh $0 stop
    sh $0 start
    ;;
esac
