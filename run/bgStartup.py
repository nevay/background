#! /usr/bin/env python

import cothread
import cothread.catools as ca
import Acq
import Data
import os
import optparse
import numpy as np
import random as rand

class Stop :
    def __init__(self):
        pass

    def RecordValues(self):
        # write current values to disk in a file.
        
        a = Acq.Acq('INI')
        a.AcquireN(1)
        a.CloseAcq()

class Start :
    def __init__(self):
        pass

    def LoadValues(self):
        """
        load PV values from latest INI file
        """
        
        localsettings  = Data.GetLocalSettings()
        inidir         = localsettings['inidir']
        if inidir[-1] != '/':
            inidir += '/'
        try:
            filename = os.listdir(inidir)[-1] #last file in directory - last set of settings
        except OSError:
            print 'No INI file - not loading anything - be careful of default values!'
            return
        filepath = inidir+filename
        print 'Reading ',filepath
        b = Data.Load(filepath)
        del b.data['refc1amp'] #remove ones we shouldn't set from this data instance
        del b.data['info']
        for key, value in b.data.iteritems():
            print 'value at start ',value
            if len(np.shape(value)) > 1:
                v = value[0]
            elif len(value) == 1:
                v = value[0]
            else:
                v = value
            print 'v',v
            print 'Startup> Start> Putting ',key,' > ',v
            ca.caput(key,v,timeout=0.4,throw=False)

def main():
    usage = ''
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--start',action='store_true',default=False)
    parser.add_option('-b','--stop',action='store_true',default=False)

    options,args = parser.parse_args()
    
    if options.start :
        print 'Loading old db values\n'
        a = Start()
        a.LoadValues()

    if options.stop :
        print 'Saving current dv values\n'
        a = Stop()
        a.RecordValues()

if __name__ == "__main__":
    main()
