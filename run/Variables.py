

class Variable:
    def __init(self,pv,dataname):
        self.pv   = pv
        self.name = dataname

variables = []

extmagnets    = [
    'QM6R',  'QM7R',  'BS1X',  'ZV1X',  'QS1X',  'QF1X',   'ZH1X',    'ZX1X',
    'ZV2X',  'QD2X',  'QF3X',  'ZH1X',  'ZV3X',  'QF4X',   'QD5X',    'ZV4X',
    'ZX2X',  'ZV5X',  'QF6X',  'QS2X',  'BH3X',  'ZV3X',   'QF7X',    'ZH3X',
    'QD8X',  'ZV6X',  'QF9X',  'ZH4X',  'ZV7X',  'QK1X',   'QD10X',   'ZH5X',
    'QF11X', 'ZV8X',  'QK2X',  'QD12X', 'ZH6X',  'QD14X',  'ZH7X',    'QF15X',
    'ZV9X',  'QK3X',  'QD16X', 'ZH8X',  'QF17X', 'QK4X',   'QD18X',   'ZH9X',
    'QF19X', 'ZV11X', 'QD20X', 'ZH10X', 'QF21X'
]

atf2magnets   = [
    'QM16FF', 'ZH1FF',  'ZV1FF',   'QM15FF', 'QM14FF', 'QM13FF', 'QM12FF', 
    'QM11FF', 'QD10BFF','QD10AFF', 'QF9BFF', 'SF6FF',  'QF9AFF', 'QD8FF', 
    'QF7FF',  'B5FF',   'QD6FF',   'QF5BFF', 'SF5FF',  'QF5AFF', 'QD4BFF',
    'SD4FF',  'QD4AFF', 'B2FF',    'QF3FF',  'B1FF',   'QD2BFF', 'QD2AFF',
    'SF1FF',  'QF1FF',  'SD0FF',   'QD0FF',  'BDUMP'
]
