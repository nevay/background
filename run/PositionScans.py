# script that analyses and plots the position scan data

import numpy as _np
import matplotlib as _mpl
import Ana

def PlotPositionScan(fileNames, positions, outputName="PositionScan", titleName=""):
    mean1=[]
    mean2=[]
    for i in fileNames:
        a = Ana.Analysis(i)
        a.X('detector1signalcn')
        a.Y('detector2signalcn')
        mean1.append(_np.mean(a.x))
        mean2.append(_np.mean(a.y))

    figure()
    print len(mean1)
    plot(positions,mean1,'b+',label='detector 1')
    plot(positions,mean2,'ro',label='detector 2')
    xlabel('position [mm]')
    ylabel('charge normalised signal')
    title(titleName)
    legend(loc=0)
    savefig(outputName+".png")


positions = _np.arange(4.5,-4.6,-0.5)

collRefHor = ["20150618_230026_log.dat","20150618_230107_log.dat","20150618_230153_log.dat","20150618_230236_log.dat","20150618_230333_log.dat","20150618_230428_log.dat","20150618_230524_log.dat","20150618_230608_log.dat","20150618_230650_log.dat","20150618_230732_log.dat","20150618_230814_log.dat","20150618_230857_log.dat","20150618_230948_log.dat","20150618_231040_log.dat","20150618_231123_log.dat","20150618_231207_log.dat","20150618_231257_log.dat","20150618_231340_log.dat","20150618_231428_log.dat"]

collRefVert = ["20150618_231724_log.dat","20150618_231949_log.dat","20150618_232037_log.dat","20150618_232117_log.dat","20150618_232205_log.dat","20150618_232302_log.dat","20150618_232347_log.dat","20150618_232433_log.dat","20150618_232515_log.dat","20150618_232557_log.dat","20150618_232643_log.dat","20150618_232726_log.dat","20150618_232817_log.dat","20150618_232909_log.dat","20150618_232949_log.dat","20150618_233041_log.dat","20150618_233122_log.dat","20150618_233208_log.dat"]

collVert = ["20150618_233552_log.dat","20150618_233631_log.dat","20150618_233710_log.dat","20150618_233752_log.dat","20150618_233834_log.dat","20150618_233913_log.dat","20150618_233953_log.dat","20150618_234037_log.dat","20150618_234117_log.dat","20150618_234156_log.dat","20150618_234235_log.dat","20150618_234319_log.dat","20150618_234358_log.dat","20150618_234437_log.dat","20150618_234516_log.dat","20150618_234555_log.dat","20150618_234634_log.dat","20150618_234713_log.dat","20150618_234752_log.dat"]

PlotPositionScan(collRefHor,positions,outputName="CollRefHorScan",titleName="horizontal position of collimator and ref. cavity")
# no point at -4.5mm
PlotPositionScan(collRefVert,positions[:-1],outputName="CollRefVertScan",titleName="vertical position of collimator and ref. cavity")
PlotPositionScan(collVert,positions,outputName="CollVertScan",titleName="vertical position of collimator")



