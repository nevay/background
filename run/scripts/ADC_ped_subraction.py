#! /usr/bin/env python

import cothread
import cothread.catools as ca
import numpy as np
import os
import sys
sys.path.append('../')

import Acq
import Data

npulses = 20

def main():
    os.chdir('../')
    a = Acq.Acq('ped',['pedestal'])
    a.AcquireN(npulses)
    a.CloseAcq()
    b = Data.Data()
    b.Read(a.filepath)
    peddata = b.data['adcdataraw']
    pedavg = np.average(peddata,axis=0)
    pedavg = pedavg.round(0)
    pedavg = np.array(pedavg,dtype=int)
    print 'ADC average background calcualted to be: ',pedavg
    ca.caput('bg:adcdata:background',pedavg)
    
    print '\nADC pedestal data updated from average of ',npulses,' pulses\n'

if __name__ == "__main__" :
    main()
