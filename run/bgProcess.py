#! /usr/bin/env python

import cothread
import cothread.catools as ca
import time
import numpy as np
import time
import Data
import signal
import sys

#create a signal handler
def signal_handler(signal, frame):
        ca.cothread.Quit()
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

class Process:
    def __init__(self):
        # trigger var should
        settings = Data.GetLocalSettings()
        try:
            self.triggervar = 'cc8:adcArray'
            #settings['triggervar']
            print 'Process> triggering using PV: ',self.triggervar
        except KeyError:
            print "Please specify a 'triggervar' key in the LOCALSETTINGS.txt file"
            return

        #for loop timing - see bottom of callback too
        #self.f = open('time_to_complete.dat','w')

        #Initialize variables
        self.detector1Index  = 4
        self.detector2Index  = 5
        self.wsdetectorIndex = 6

        # alive time
        self.nPulseAlive = 0

        # quit event
        self.quitEvent = cothread.Event(auto_reset = False)
            
    def monitor(self):
        s = ca.camonitor(self.triggervar,self.callback)
        self.quitEvent.Wait()
        
    def callback(self,value):
        t = time.time()
        print 'Process> Callback',self.nPulseAlive,'\ttime (s):',t

        # GET THINGS
        # get all variables at once to minimise caget overhead
        # approximately independent vs number of pvs to get
        pvs_to_get = [
                'cc8:adcArray',
                'bg:adcdata:background'
                ]
        cc8adc,adcbg = ca.caget(pvs_to_get,timeout=0.1,throw=False)
        
        pvs_to_put = []
        val_to_put = []
        
        cc8adcshort = cc8adc[4:-3]
        
        #copy raw adc data
        pvs_to_put.append("bg:adcdata:raw")
        val_to_put.append(cc8adcshort)

        bgsubtracted = cc8adcshort - adcbg
        pvs_to_put.append("bg:adcdata:signal")
        val_to_put.append(bgsubtracted)

        # split apart cc8 adc array and put in detector pvs
        #detector1
        pvs_to_put.append("bg:detector1:adcraw")
        val_to_put.append(cc8adcshort[0])

        pvs_to_put.append("bg:detector1:background")
        #note our background array doesn't have any rubbish at beginning
        val_to_put.append(adcbg[0])

        pvs_to_put.append("bg:detector1:signal")
        val_to_put.append(bgsubtracted[0])
        
        #detector2
        pvs_to_put.append("bg:detector2:adcraw")
        val_to_put.append(cc8adcshort[1])
        
        pvs_to_put.append("bg:detector2:background")
        #note our background array doesn't have any rubbish at beginning
        val_to_put.append(adcbg[1])
        
        pvs_to_put.append("bg:detector2:signal")
        val_to_put.append(bgsubtracted[1])

        #put all pvs and values in one go (lower overhead)
        ca.caput(pvs_to_put,val_to_put,timeout=0.1,throw=False)
        
        # Increment counter for uptime check
        self.nPulseAlive = self.nPulseAlive + 1
        #cothread.catools.ca_flush_io()

def main() :
    p = Process()
    p.monitor()
    
if __name__ == "__main__" :
    main() 
    
