import Ana
import numpy as np

files = {
    0    : '20150618_191819_log.dat',
    700  : '20150618_191501_log.dat',
    801  : '20150618_184726_log.dat',
    900  : '20150618_185618_log.dat',
    1000 : '20150618_190010_log.dat',
    1100 : '20150618_190318_log.dat',
    1200 : '20150618_190757_log.dat',
    1300 : '20150618_191146_log.dat'
}

def Analyse(prefix = ''):
    voltages = np.sort(files.keys())
    data = []
    for voltage in voltages:
        a,b,c,d,e,f = IndividualAnalysis(prefix+files[voltage])
        data.append([voltage,a,b,c,d,e,f])
    data = np.array(data)
    return data

def IndividualAnalysis(filename):
    a = Ana.Analysis(filename)
    a.X('detector1raw')
    m1   = np.mean(a.x)
    std1 = np.std(a.x)
    err1 = std1/np.sqrt(len(a.x))
    a.X('detector2raw')
    m2   = np.mean(a.x)
    std2 = np.std(a.x)
    err2 = std2/np.sqrt(len(a.x))
    return m1,std1,err1,m2,std2,err2

def Plot(prefix=''):
    data = Analyse(prefix)
    plt.figure()
    plt.errorbar(dat[:,0],dat[:,1],yerr=dat[:,3],fmt='.',label='Detector 1')
    plt.errorbar(dat[:,0],dat[:,4],yerr=dat[:,6],fmt='.',label='Detector 2')
    plt.legend()
    plt.xlabel('Voltage (V)')
    plt.ylabel('ADC Counts')
    plt.title('Background with No Beam')
    plt.xlim(-30,1340)
    plt.ylim(120,200)
