#!../../bin/linux-x86_64/ioc

## You may have to change ioc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/ioc.dbd"
ioc_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/detector.db","NAME=bg:detector1")
dbLoadRecords("db/detector.db","NAME=bg:detector2")
dbLoadRecords("db/bg.db")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=rhul"
