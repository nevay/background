# RHUL Cherenkov Background DAQ #

Author: L. Nevay

DAQ software for read out, data recording and analysis for rhul cherenkov background monitors at the ATF2 at KEK.

![Screen Shot 2015-06-17 at 17.46.34.png](https://bitbucket.org/repo/jrep4X/images/4187186184-Screen%20Shot%202015-06-17%20at%2017.46.34.png)